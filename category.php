<?php
/**
 * The main template file
 */

get_header();

$category_object  = get_queried_object();
$featured_content = get_field('featured_content', $category_object);
$term_link = get_term_link($category_object);

$page_header = get_field('page_header', $category_object);

$header_args = array(
    'title' => $category_object->name,
    'description' => $category_object->description,
    'media_column' => 'full',
    'id' => 'page-header',
);

if(!empty($page_header['primary_button'])) {
    $header_args['primary_button'] = $page_header['primary_button'];
}

if(!empty($page_header['secondary_button'])) {
    $header_args['secondary_button'] = $page_header['secondary_button'];
}

get_template_part(
	'template-parts/elements/flexible-page-header', 
	null,
    $header_args
);
?>
<div class="vil-block vil-featured-content vil-section-element">
	<div class="container">
		<div class="vil-featured-content__wrap">
			<div class="vil-main-post__top">
				<a class="vil-main-post__back" href="<?php echo get_permalink( get_option('page_for_posts') ); ?>">
					<img src="<?php echo V_TEMP_URL . '/assets/img/back-post.svg'; ?>" alt="">
				</a>
				<div class="vil-main-post__top-links">
					<a class="vil-main-post__top-link" href="<?php echo get_permalink( get_option('page_for_posts') ); ?>"> Articles & Resources > </a>
					<p class="vil-main-post__current-post"><?php echo $category_object->name; ?></p>
				</div>
			</div>

			<?php if ( ! empty( $featured_content['enable'] ) && get_query_var('paged') < 2 && ! isset( $_GET['order'] ) ):
				$f_post_id = $featured_content['featured_post'];
				$featured_args_right = array(
					'post_type' => 'post',
					'post_status' => 'publish',
					'posts_per_page' => -1,
					'post__in' => $featured_content['posts'],
					'orderby' => 'post__in'
				); 

				$query_posts = new WP_Query($featured_args_right); ?>
				
				<h2 class="vil-featured-content__title">
					<?php echo $featured_content['title']; ?>
				</h2>

				<div class="row">
					<div class="col-lg-6 col-md-12 col-sm-12">
						<?php $time_to_read = get_field('time_to_read', $f_post_id); ?>
						<div class="vil-featured-content__post">
							<?php if ( has_post_thumbnail( $f_post_id ) ): ?>
								<div class="vil-main-post__content-image-wrap">
									<img class="vil-main-post__content-image" src="<?php echo get_the_post_thumbnail_url($f_post_id); ?>" alt="">
								</div>
							<?php endif ?>

							<div class="vil-featured-content__three-posts-card-top">
								<?php if ( ! empty( get_the_category( $f_post_id ) ) ) : ?>
									<?php foreach ((get_the_category($f_post_id)) as $category): ?>
										<div class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></div>
									<?php endforeach; ?>
								<?php endif; ?>
								<?php if ( ! empty($time_to_read ) ) : ?>
									<img class="vil-articles__card-clock" src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/icon-clock-grey.svg" alt="">
									<span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
								<?php endif; ?>
							</div>
							<h4 class="vil-featured-content__title-post">
								<a href="<?php echo get_the_permalink( $f_post_id ); ?>"><?php echo get_the_title( $f_post_id ); ?></a>
							</h4>
							<div class="vil-main-post__content">
								<p><?php echo get_the_excerpt($f_post_id); ?></p>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-12 col-sm-12 pl-xl-40">
						<div class="vil-featured-content__three-posts">
							<?php
							while ( $query_posts->have_posts() ) : $query_posts->the_post();
								$time_to_read = get_field('time_to_read', get_the_ID());
								?>
								<div class="vil-featured-content__three-posts-card">
									<div class="row">
										<div class="col-lg-5 col-md-5 col-sm-5">
											<?php if ( has_post_thumbnail() ): ?>
												<div class="vil-featured-content__three-posts-card-image-wrap">
													<img class="vil-featured-content__three-posts-card-image" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="image">
												</div>
											<?php endif ?>
										</div>
										<div class="col-lg-7 col-md-7 col-sm-7">
											<div class="vil-featured-content__three-posts-card-content">
												<div class="vil-featured-content__three-posts-card-top">
													<?php if (!empty(get_the_category())) : ?>
														<?php foreach ((get_the_category()) as $category): ?>
															<div class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></div>
														<?php endforeach; ?>
													<?php endif; ?>

													<?php if (!empty($time_to_read)) : ?>
														<img class="vil-articles__card-clock"
															 src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/icon-clock-grey.svg"
															 alt="">

														<span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
													<?php endif; ?>
												</div>
												<a href="<?php the_permalink(); ?>" class="vil-featured-content__three-posts-card-title"><?php the_title(); ?></a>
											</div>
										</div>
									</div>
								</div>
							<?php endwhile;
							wp_reset_postdata(); ?>
						</div>
					</div>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>

<div class="vil-more-posts vil-section-element">
	<div class="container">
		<div class="vil-more-posts__top vil-more-posts__categories-select-full">
			<h3 class="vil-more-posts__title">
				<span><?php echo $category_object->name; ?></span>
				<?php esc_html_e('Articles & Resources'); ?>
			</h3>
			<div class="select-wrap">
				<select class="vil-search__select select2 category-sort" name="select" style="width: 100%">
					<option class="vil-search__select-item" value="<?php echo $term_link; ?>">Sort Results</option>
					<option class="vil-search__select-item" value="<?php echo $term_link; ?>">Newest</option>
					<option class="vil-search__select-item" value="<?php echo $term_link; ?>?order=asc">Oldest</option>
					<option class="vil-search__select-item" value="<?php echo $term_link; ?>?order=read-time">Quick Reads</option>
				</select>
			</div>
		</div>

		<div class="vil-more-posts__wrap js-posts-wrap" data-slug="<?php echo $category_object->slug; ?>">
			<div class="row category-posts-container">
				<?php if ( have_posts() ) :
					while ( have_posts() ) : the_post();
                        get_template_part('template-parts/elements/single-post', null, array('layout' => 'category'));
					endwhile;
				endif; ?>
			</div>

            <div class="vil-more-posts__pagination">
                <?php the_posts_pagination(array(
                        'prev_next' => false,
                    )
                ); ?>
            </div>
		</div>
	</div>
</div>


<?php
get_footer();

