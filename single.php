<?php
/**
 * Single post template
 */

$post_id = get_the_ID();
$related_title = get_field('related_title');
$related_content = get_field('related_content');
$related_programs = get_field('related_programs');
$time_to_read = get_field('time_to_read');
$gd_guide = get_field('gd_guide');

$args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 4,
    'post__not_in' => array($post_id)
);
$query_posts = new WP_Query($args);

$post_terms = wp_get_post_terms($post_id, 'category');

$terms = get_terms( array(
    'taxonomy'   => 'category',
    'hide_empty' => true,
) );

get_header();

if (have_posts()) :
    while (have_posts()) : the_post(); ?>
        <div class="vil-progress-container vil-section-element">
            <div class="vil-progress-bar" id="vil-progress"></div>
        </div>
        <div class="vil-block vil-section-element">
            <div class="container">
                <div class="vil-main-post__top">
                    <a class="vil-main-post__back" href="<?php echo get_permalink( get_option('page_for_posts') ); ?>">
                        <img src="<?php echo V_TEMP_URL . '/assets/img/back-post.svg'; ?>" alt="">
                    </a>
                    <div class="vil-main-post__top-links">
                        <a class="vil-main-post__top-link" href="<?php echo get_permalink( get_option('page_for_posts') ); ?>"> Articles & Resources > </a>
                        <?php if ( ! empty( $post_terms ) ): ?>
                            <a class="vil-main-post__top-link" href="<?php echo get_term_link( $post_terms[0] ); ?>"><?php echo $post_terms[0]->name; ?> > </a>
                        <?php endif ?>
                        <p class="vil-main-post__current-post"><?php the_title(); ?></p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="vil-main-post__content">
                            <div class="vil-main-post__content-info">
                                <?php if (!empty(get_the_category())) : ?>
                                    <?php foreach ((get_the_category()) as $category): ?>
                                        <span class="vil-main-post__sidebar-articles-category">
                                            <a href="<?php echo get_term_link($category) ?>">
                                                <?php echo $category->name; ?>
                                            </a>
                                        </span>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                                <?php if ( ! empty( $time_to_read ) ) : ?>
                                    <div>
                                        <img class="vil-articles__card-clock"
                                             src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/icon-clock-grey.svg"
                                             alt="">
                                        <span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
                                    </div>
                                <?php endif; ?>
                            </div>

                            <?php the_title('<h1 class="vil-main-post__content-title">', '</h1>'); ?>

                            <?php if ( has_post_thumbnail() ) : ?>
                                <div class="vil-main-post__content-image-wrap">
                                    <?php the_post_thumbnail('large', array('class' => 'vil-main-post__content-image')); ?>
                                </div>
                            <?php endif; ?>

                            <p class="vil-main-post__content-post-data">
                                 <?php echo esc_html__('Last Updated ', V_PREFIX) . get_the_modified_date(get_option('date_format')); ?>
                            </p>

                            <div class="vil-main-post__main">
                                <?php the_content(); ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="vil-main-post__sidebar">

                            <?php if ( ! empty( $terms ) ): ?>
                                <div class="select-wrap">
                                    <select class="vil-search__select select2- js-cats" name="select">
                                        <option class="vil-search__select-item" value="value1">Browse Categories</option>
                                        <?php foreach ($terms as $term): ?>
                                            <option class="vil-search__select-item" value="<?php echo get_term_link($term); ?>"><?php echo $term->name ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            <?php endif ?>

                            <?php
                            /**
                             * Guide Download
                             */

                            $guide_id = get_the_id();
                            $guide_data = array();

                            if ( ! empty( $gd_guide ) ) {
                                $guide_id = $gd_guide;
                            } else {
                                if ( ! empty( $post_terms ) ) {
                                    foreach($post_terms as $post_term ) {
                                        $gd_guide = get_field('gd_guide', $post_term);
                                        if ( ! empty( $gd_guide ) ) {
                                            $guide_id = $gd_guide;
                                        }
                                    }
                                }
                            }

                            $guide_query = new WP_Query($args);

                            if ( ! empty( $guide_id ) ) :
                                $post_id = $guide_id;
                                $title = get_field('title', $post_id);
                                $subtitle = get_field('subtitle', $post_id);
                                $text = get_field('text', $post_id);
                                $image = get_field('image', $post_id);
                                $cta = get_field('cta', $post_id);
                                $guide_id = get_field('guide_id', $post_id);

                                // Guide data
                                if ( ! empty( $guide_id ) ) {
                                    $guide_data['guide_id'] = $guide_id['id'];
                                    $guide_data['title'] = $title;
                                    $guide_data['subtitle'] = $subtitle;
                                    $guide_data['text'] = $text;
                                    $guide_data['image'] = $image;
                                }
                                ?>
                                <div class="vil-main-post__sidebar-guide">
                                    <div class="vil-main-post__sidebar-guide-title"><?php echo esc_html( $title ); ?></div>

                                    <?php if ( ! empty( $image ) ) : ?>
                                        <img class="vil-main-post__sidebar-guide-img" src="<?php echo esc_url( $image['url'] ); ?>" alt="">
                                    <?php endif; ?>

                                    <a href="#" class="vil-main-post__sidebar-guide-btn vil-btn vil-btn_third vil-btn_small js-show-modal" data-modal="download"><?php echo esc_html( $cta ); ?></a>
                                </div>
                            <?php endif; ?>

                            <?php 
                            /**
                             * Related Programs
                             */

                            $programs_ids = array();
                            if ( ! empty( $related_programs ) ) {
                                $programs_ids = $related_programs;
                            } else {
                                if ( ! empty( $post_terms ) ) {
                                    foreach($post_terms as $post_term ) {
                                        $related_programs = get_field('related_programs', $post_term);
                                        if ( ! empty( $related_programs ) ) {
                                            $programs_ids = $related_programs;
                                        }
                                    }
                                }
                            }

                            if ( ! empty( $programs_ids ) ): ?>
                                <div class="vil-main-post__sidebar-programs">
                                    <h6 class="vil-main-post__related-title"><?php esc_html_e('Related Programs', V_PREFIX); ?></h6>
                                    
                                    <?php foreach ($programs_ids as $program_id):
                                        $listing_description = get_field('listing_description', $program_id); ?>
                                        <div class="vil-main-post__sidebar-programs-card">
                                            <?php if ( has_post_thumbnail( $program_id ) ): ?>
                                                <img class="vil-main-post__sidebar-programs-img" src="<?php echo get_the_post_thumbnail_url( $program_id ); ?>" alt="">
                                            <?php endif ?>

                                            <div class="vil-main-post__sidebar-programs-title">
                                                <a class="vil-main-post__sidebar-programs-title-link" href="<?php echo get_the_permalink($program_id) ?>">
                                                    <?php echo get_the_title($program_id) ?>
                                                </a>
                                            </div>
                                            
                                            <?php if ( ! empty( $listing_description ) ): ?>
                                                <div class="vil-main-post__sidebar-programs-text"><?php echo $listing_description; ?></div>
                                            <?php endif ?>
                                            
                                            <a href="<?php echo get_the_permalink($program_id) ?>" class="vil-main-post__sidebar-programs-btn" title="@@textBtnBorder">
                                                <?php esc_html_e('Program Details', V_PREFIX); ?>
                                            </a>
                                        </div>
                                    <?php endforeach ?>
                                </div>
                            <?php endif ?>

                            <?php
                            /**
                             * Related Articles
                             */

                            if ( $query_posts->have_posts() ) : ?>
                                <div class="vil-main-post__sidebar-articles">
                                    <h6 class="vil-main-post__related-title"><?php esc_html_e('Related Articles', V_PREFIX); ?></h6>

                                    <?php while ($query_posts->have_posts()) : $query_posts->the_post(); ?>
                                        <div class="vil-main-post__sidebar-articles-card">
                                            <div class="vil-main-post__sidebar-articles-text-wrap">
                                                <?php if (!empty(get_the_category())) : ?>
                                                    <?php foreach ((get_the_category()) as $category): ?>
                                                        <a href="<?php echo get_term_link($category); ?>" class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></a>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                                <div class="vil-main-post__sidebar-articles-title-wrap">
                                                    <a class="vil-main-post__sidebar-articles-title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                                </div>
                                            </div>
                                            <div class="vil-main-post__sidebar-articles-img-wrap">
                                                <img class="vil-main-post__sidebar-articles-img"
                                                     src="<?php echo get_the_post_thumbnail_url(); ?>"
                                                     alt="">
                                            </div>
                                        </div>
                                    <?php endwhile; wp_reset_postdata(); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
            /**
             * Related Content
             */
            ?>
            <div class="vil-main-post__related">
                <div class="container">
                    <?php if ( ! empty( $related_content ) ) : ?>
                        <div class="row">
                            <div class="col-lg-8 col-md-12 col-sm-12">
                                <div class="vil-main-post__related-wrap">
                                    <?php if (!empty($related_title)) : ?>
                                        <h6 class="vil-main-post__related-title"><?php echo $related_title; ?></h6>
                                    <?php endif; ?>

                                    <?php foreach ($related_content as $row): ?>
                                        <?php $title = $row['title']; ?>
                                        <?php $description = $row['description']; ?>
                                        <?php $link = $row['link']; ?>
                                        <?php $image = $row['image']; ?>
                                        <div class="vil-main-post__related-post">
                                            <?php if (!empty($image)): ?>
                                                <div class="vil-main-post__related-post-image">
                                                    <img class="vil-main-post__related-post-image-picture"
                                                         src="<?php echo esc_url($image['url']); ?>"
                                                         alt="<?php echo $image['alt'] ?>">
                                                </div>
                                            <?php endif; ?>
                                            <div class="vil-main-post__related-content">
                                                <?php if (!empty($title)) : ?>
                                                    <h6 class="vil-main-post__related-content-title"><?php echo $title; ?></h6>
                                                <?php endif; ?>
                                                <?php if (!empty($description)) : ?>
                                                    <div class="vil-main-post__related-content-text">
                                                        <?php echo $description; ?>
                                                    </div>
                                                <?php endif; ?>
                                                <?php vil_get_button($link, 'vil-main-post__related-content-more'); ?>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php
    endwhile;
endif;

/**
 * Include download modal
 */
if ( ! empty( $guide_data ) ) {
    get_template_part('template-parts/modals/download', null, $guide_data);
}

get_footer();