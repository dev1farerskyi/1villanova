<?php
/**
 * Certificate programs archive template file
 */

get_header();


$page_header = get_field('cp_page_header', 'options');
$cp_faq 	 = get_field('cp_faq', 'options');
$programs 	 = get_field('cp_certificate_programs', 'options');

$rt_enroll   = get_field('p_ready_to_enroll', 'options');
$rt_enroll['id'] = 'ready-to-enroll';
$rt_enroll['wrapper_class'] = 'vil-ready-to-enroll__space-light';

get_template_part(
	'template-parts/elements/flexible-page-header', 
	null, 
	array(
		'title' => $page_header['title'], 
		'description' => $page_header['description'], 
		'primary_button' => $page_header['primary_button'], 
		'secondary_button' => $page_header['secondary_button'], 
		'media_column' => 'full',
        'id' => 'page-header'
	)
);


get_template_part(
    'template-parts/blocks/certificate-programs-listing', 
    null, 
    array(
        'title' => $programs['title'],
        'anchor_title' => $programs['anchor_title'],
        'display_order' => $programs['display_order'], 
        'items'  => $programs['items'],
        'anchor' => 'programs'
    )
);


get_template_part( 'template-parts/blocks/ready-to-enroll', null, $rt_enroll );

if ( ! empty( $cp_faq['items'] ) ) {
    get_template_part(
        'template-parts/blocks/faq',
        null,
        array(
            'title'  => $cp_faq['title'],
            'anchor_title' => $cp_faq['anchor_title'],
            'items'  => $cp_faq['items'],
            'style'  => 'style-1',
            'anchor' => 'faq'
        )
    );
}

get_footer();
