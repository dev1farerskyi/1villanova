<?php
/**
 * Header template.
 *
 * @package villanova
 * @since 1.0.0
 *
 */

$main_logo    = get_field('main_logo', 'option');

$top_menu 	  = get_field('top_menu', 'option');
$menu_title   = get_field('contat_menu_title', 'option');
$contact_menu = get_field('contact_menu', 'option');
$other_links  = get_field('other_links', 'option');
$request_demo = get_field('request_demo', 'option');
$mobile_nav_buttons = get_field('mobile_nav_buttons', 'option');
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no"/>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header class="vil-header">
    <?php if ( $top_menu ): ?>
        <?php get_template_part('template-parts/announcement_bar') ?>
        <div class="vil-header__top">
            <div class="container-fluid">
                <div class="d-flex vil-header__top-wrap">
                    <div class="vil-header__search">
                        <form action="<?php echo site_url('/'); ?>" method="get">
                            <input type="text" placeholder="" name="s">
                            <button type="submit" class="vil-btn vil-header__search-btn">
                                <svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10.7204 9.93396H10.0429L9.80274 9.7024C10.6432 8.7247 11.1492 7.4554 11.1492 6.07461C11.1492 2.99571 8.65352 0.5 5.57461 0.5C2.49571 0.5 0 2.99571 0 6.07461C0 9.15352 2.49571 11.6492 5.57461 11.6492C6.9554 11.6492 8.2247 11.1432 9.2024 10.3027L9.43396 10.5429V11.2204L13.7221 15.5L15 14.2221L10.7204 9.93396ZM5.57461 9.93396C3.43911 9.93396 1.71527 8.21012 1.71527 6.07461C1.71527 3.93911 3.43911 2.21527 5.57461 2.21527C7.71012 2.21527 9.43396 3.93911 9.43396 6.07461C9.43396 8.21012 7.71012 9.93396 5.57461 9.93396Z" fill="#fff" />
                                </svg>
                            </button>
                        </form>
                    </div>

                    <div class="vil-header__dropdown">
                        <div class="vil-header__dropdown-head"><?php echo $menu_title; ?></div>
                        <?php if ( ! empty( $contact_menu ) ): ?>
                            <div class="vil-header__dropdown-body">
                                <?php foreach ($contact_menu as $item):
                                    if ( ! empty( $item['link'] ) ):
                                        $custom_class = ! empty( $item['custom_class'] ) ? $item['custom_class'] : '';
                                        $item['link']['title'] = ! empty( $item['svg_icon_code'] ) ? '<span class="vil-header__contact-icon">' . $item['svg_icon_code'] . '</span>' . $item['link']['title'] : $item['link']['title'];
                                        vil_get_button( $item['link'], 'vil-header__contact ' . $custom_class); ?>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <?php if ( ! empty( $request_demo ) ) :
                        $classes = array('vil-header__link');

                        vil_get_button( $request_demo, 'vil-header__link d-sm-none');
                    endif; ?>

                    <?php if ( ! empty( $other_links ) ): ?>
                        <?php foreach ($other_links as $link):
                            $classes = array('vil-header__link');
                            $classes[] = $link['yellow'] ? 'link_check-in' : '';
                            $classes[] = $link['hide_on_mobile'] ? 'd-none d-sm-block' : '';

                            vil_get_button( $link['link'], implode(' ', $classes )); ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </div>
            </div>
        </div>
    <?php endif ?>

    <div class="container-fluid">
        <div class="d-flex vil-header__main">
            <?php if ( ! empty( $main_logo ) ): ?>
                <a href="<?php echo site_url('/'); ?>" class="vil-header__logo">
                    <img src="<?php echo $main_logo['url']; ?>" alt="<?php echo $main_logo['alt']; ?>">
                </a>
            <?php endif ?>

            <button class="vil-header__burger">
                <span></span>
                <span></span>
                <span></span>
            </button>

            <nav class="vil-header__nav">
                <div class="vil-header__search vil-header__search_mob">
                    <form action="<?php echo site_url('/'); ?>" method="get">
                        <input type="text" placeholder="Search" name="s">
                        <button type="submit" class="vil-btn vil-header__search-btn">
                            <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M11.8368 10.3756L9.49991 8.03867C9.39444 7.9332 9.25146 7.8746 9.10145 7.8746H8.71939C9.36631 7.04719 9.75071 6.00649 9.75071 4.87438C9.75071 2.18121 7.56852 -0.000976562 4.87536 -0.000976562C2.18219 -0.000976562 0 2.18121 0 4.87438C0 7.56754 2.18219 9.74974 4.87536 9.74974C6.00747 9.74974 7.04817 9.36533 7.87558 8.71841V9.10047C7.87558 9.25048 7.93417 9.39346 8.03965 9.49894L10.3765 11.8358C10.5969 12.0562 10.9531 12.0562 11.1711 11.8358L11.8345 11.1725C12.0548 10.9522 12.0548 10.5959 11.8368 10.3756ZM4.87536 7.8746C3.2182 7.8746 1.87514 6.53388 1.87514 4.87438C1.87514 3.21723 3.21586 1.87416 4.87536 1.87416C6.53251 1.87416 7.87558 3.21488 7.87558 4.87438C7.87558 6.53153 6.53485 7.8746 4.87536 7.8746Z" fill="#727272"/>
                            </svg>
                        </button>
                    </form>
                </div>
                <?php wp_nav_menu(
                    array(
                        'container' => '',
                        'items_wrap' => '<ul class="vil-header__menu">%3$s</ul>',
                        'theme_location' => 'main-menu',
                        'depth' => 3,
                        'fallback_cb' => '__return_empty_string',
                    )
                ); ?>

                <?php if ( ! empty($mobile_nav_buttons) ): ?>
                    <div class="vil-header__mobile-btns">
                        <?php foreach ( $mobile_nav_buttons as $btn ): 
                            $link_url = $btn['link']['url'];
                            $link_title = $btn['link']['title'];
                            $link_target = $btn['link']['target'] ? $btn['link']['target'] : '_self';
                        ?>
                            <a href="<?php echo $link_url ?>" class="vil-btn vil-btn_third vil-btn_small" target="<?php echo $link_target ?>">
                                <?php echo $link_title ?>
                            </a>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            </nav>
        </div>
    </div>
</header>
<main class="main-wrapper">