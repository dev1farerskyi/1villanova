<?php
/**
 * The main template file
 */

get_header();

$featured_content = get_field('featured_content', 'options');
$display_order = $featured_content['display_order'];
$featured_args_left = array(
    'post_type' => 'post',
);

$featured_args_right = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 3,
);

if (!empty($featured_content['featured_post'])) {
    $featured_args_left['post__in'] = array($featured_content['featured_post']);
    $featured_args_right['post__not_in'] = array($featured_content['featured_post']);
}

if ($display_order === 'manual') {
    $featured_args_right['post__in'] = $featured_content['posts'];
}
?>
<?php
$page_header = get_field('page_header', 'options');

get_template_part('template-parts/blocks/flexible-page-header', null, array('title' => $page_header['title'], 'description' => $page_header['description'], 'primary_button' => $page_header['primary_button']));

?>
    <div class="vil-block vil-featured-content vil-section-element">
        <div class="container">
            <div class="vil-featured-content__wrap">
                <?php if (!empty($featured_content['title'])) :
                    $anchor_title = ! empty( $featured_content['anchor_title'] ) ? 'id="' . $featured_content['anchor_title'] . '"' : '';
                    ?>
                    <div class="vil-featured-content__up">
                        <h2 <?php echo $anchor_title; ?> class="vil-featured-content__title">
                            <?php echo $featured_content['title']; ?>
                        </h2>
                    </div>
                <?php endif ?>

                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <?php
                        $featured_query = new WP_Query($featured_args_left); ?>
                        <?php if ($featured_query->have_posts()) : ?>
                            <?php while ($featured_query->have_posts()) : $featured_query->the_post();
                                $time_to_read = get_field('time_to_read', get_the_ID());
                                ?>
                                <div class="vil-featured-content__post">
                                    <div class="vil-main-post__content-image-wrap">
                                        <img class="vil-main-post__content-image"
                                             src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                                    </div>
                                    <div class="vil-featured-content__three-posts-card-top">
                                        <?php if (!empty(get_the_category())) : ?>
                                            <?php foreach ((get_the_category()) as $category): ?>
                                                <a href="<?php echo get_term_link($category); ?>" class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></a>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <?php if (!empty($time_to_read)) : ?>
                                            <img class="vil-articles__card-clock"
                                                 src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/icon-clock-grey.svg"
                                                 alt="">
                                            <span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <h4 class="vil-featured-content__title-post">
                                        <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                                    </h4>
                                    <div class="vil-main-post__content">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 pl-xl-40">
                        <div class="vil-featured-content__three-posts">
                            <?php
                            $featured_query_posts = new WP_Query($featured_args_right);
                            while ($featured_query_posts->have_posts()) : $featured_query_posts->the_post();
                                $time_to_read = get_field('time_to_read', get_the_ID());
                                ?>
                                <div class="vil-featured-content__three-posts-card">
                                    <div class="row">
                                        <?php if ( has_post_thumbnail(get_the_ID()) ) : ?>
                                            <div class="col-lg-5 col-md-5 col-sm-5">
                                                <div class="vil-featured-content__three-posts-card-image-wrap">
                                                    <img class="vil-featured-content__three-posts-card-image"
                                                         src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="image">
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <div class="<?php echo has_post_thumbnail(get_the_ID()) ? 'col-lg-7 col-md-7 col-sm-7' : 'col-12'; ?>">
                                            <div class="vil-featured-content__three-posts-card-content">
                                                <div class="vil-featured-content__three-posts-card-top">
                                                    <?php if (!empty(get_the_category())) : ?>
                                                        <?php foreach ((get_the_category()) as $category): ?>
                                                            <a href="<?php echo get_term_link($category); ?>" class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></a>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                    <?php if (!empty($time_to_read)) : ?>
                                                        <img class="vil-articles__card-clock"
                                                             src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/icon-clock-grey.svg"
                                                             alt="">

                                                        <span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
                                                    <?php endif; ?>
                                                </div>
                                                <a href="<?php the_permalink(); ?>"
                                                   class="vil-featured-content__three-posts-card-title"><?php the_title(); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile;
                            wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="vil-more-posts vil-section-element">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="select-wrap d-block d-lg-none mb-35">
                        <select class="vil-search__select select-mobile select2 select-category" style="width: 100%">
                            <option class="vil-search__select-item" value="<?php echo get_post_type_archive_link('post'); ?>">
                                <?php esc_html_e('View All', V_PREFIX); ?>
                            </option>
                            <?php foreach ((get_categories()) as $category): ?>
                                <option class="vil-search__select-item" value="<?php echo get_category_link($category->term_id); ?>">
                                    <?php echo $category->name; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="vil-more-posts__categories">
                        <p class="vil-more-posts__categories-title"><?php esc_html_e('Browse Categories', V_PREFIX); ?></p>
                        <ul class="vil-more-posts__categories-wrap">
                            <li class="vil-more-posts__categories-item">
                                <a href="<?php echo get_post_type_archive_link('post'); ?>" class="active" data-slug="all"><?php esc_html_e('View All', V_PREFIX); ?></a>
                            </li>
                            <?php foreach ((get_categories()) as $category): ?>
                                <li class="vil-more-posts__categories-item">
                                    <a href="<?php echo get_category_link($category->term_id); ?>" data-slug="<?php echo $category->slug; ?>"><?php echo $category->name; ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="vil-more-posts__wrap">
                        <div class="vil-more-posts__top vil-more-posts__categories-select">
                            <h3 class="vil-more-posts__title"><?php esc_html_e('The Latest', V_PREFIX); ?></h3>

                            <a class="vil-btn vil-btn_third vil-btn_small d-none d-md-block js-show-all-posts" href="#" target="_self"><?php esc_html_e('View All', V_PREFIX); ?></a>
                        </div>
                        <div class="js-posts-wrap">
                            <div class="row">
                                <?php if (have_posts()) :
                                    while (have_posts()) : the_post();?>
                                        <?php get_template_part('template-parts/elements/single-post', null, array('layout' => 'blog')); ?>
                                    <?php endwhile; ?>
                                <?php endif; ?>
                            </div>
                            <div class="vil-more-posts__pagination">
                                <?php the_posts_pagination(array(
                                        'prev_next' => false,
                                    )
                                ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
