<?php

/*
 * Block Name: Spacer
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'vil-spacer';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;

// Create class attribute allowing for custom "className" and "align" values.
$className = array();

$className[] = $block_name;
if (!empty($block['className'])) {
    $className[] = $block['className'];
}
if (!empty($block['align'])) {
    $className[] = 'align-' . $block['align'];
}
if (!empty($is_preview)) {
    $className[] = $block_name . '_is-preview';
}

$value = get_field('value');
$value = ! empty( $args['value'] ) ? $args['value'] : $value;

$adaptive = get_field('adaptive');
$adaptive = ! empty( $args['adaptive'] ) ? $args['adaptive'] : $adaptive;

$value_768px = get_field('value_768px');
$value_768px = ! empty( $args['value_768px'] ) ? $args['value_768px'] : $value_768px;

$value_1200px = get_field('value_1200px');
$value_1200px = ! empty( $args['value_1200px'] ) ? $args['value_1200px'] : $value_1200px;

$value_1400px = get_field('value_1400px');
$value_1400px = ! empty( $args['value_1400px'] ) ? $args['value_1400px'] : $value_1400px;

?>
<style>
    <?php
    if(!empty($value)) {
    echo '#' . $id . '{height: ' . $value . 'px}';
    }

    if (!empty($adaptive)) {
        if (!empty($value_768px)) { ?>
    @media (min-width: 768px) {
    <?php echo '#' . $id . '{height: ' . $value_768px . 'px}'; ?>
    }

    <?php }
    if (!empty($value_1200px)) { ?>
    @media (min-width: 1200px) {
    <?php echo '#' . $id . '{height: ' . $value_1200px . 'px}'; ?>
    }

    <?php }
    if (!empty($value_1400px)) { ?>
    @media (min-width: 1400px) {
    <?php echo '#' . $id . '{height: ' . $value_1400px . 'px}'; ?>
    }

    <?php }
} ?>
</style>

<div id="<?php echo esc_attr($id); ?>"
     class="<?php echo esc_attr(implode(' ', $className)) ?>"></div>
