<?php

/*
 * Block Name: Ready to enroll
 * Slug:
 * Description:
 * Keywords:
 * Align: true
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$block_name = 'vil-ready-to-enroll';

$text = get_field('text');
$text = ! empty( $args['text'] ) ? $args['text'] : $text;

$button = get_field('button');
$button = ! empty( $args['button'] ) ? $args['button'] : $button;


// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : $block_name;
if (!empty($block['anchor'])) {
	$id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;

// Create class attribute allowing for custom "className" and "align" values.
$className = array( $block_name, 'vil-section-element' );

if (!empty($block['className'])) {
	$className[] = $block['className'];
}
if (!empty($block['align'])) {
	$className[] = 'align-' . $block['align'];
}
if ( ! empty( $is_preview ) ) {
	$className[] = $block_name . '_is-preview';
}

if ( empty( $text ) || empty( $button ) ) {
	$className[] = 'vil-ready-to-enroll__single';
}

$wrapper_class = '';
if ( ! empty( $args['wrapper_class'] ) ) {
	$wrapper_class = $args['wrapper_class'];
}


if ( ! empty( $text ) || ! empty( $button ) ): ?>
	<div class="<?php echo $wrapper_class; ?>">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10">
					<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr(implode(' ', $className)) ?>">
						<?php if ( ! empty( $text ) ): ?>
							<p><?php echo $text; ?></p>
						<?php endif ?>
						<?php vil_get_button( $button, 'vil-btn vil-btn_primary'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>
