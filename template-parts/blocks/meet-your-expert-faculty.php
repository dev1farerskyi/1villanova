<?php
/*
 * Block Name: Meet Your Expert Faculty Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('meet_title');
$items = get_field('items');
$items = ! empty( $args['items'] ) ? $args['items'] : $items;

$block_name = 'vil-about-program';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : $block_name;
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;

$args = array(
    'post_type' => 'faculty',
    'post_status' => 'publish',
    'post_per_page' => -1,
    'post__in' => $items,
);
$items_query = new WP_Query($args);

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'vil-section-element';
$className[] = 'faculty-padding';
?>

<?php if ($items_query->have_posts()) : ?>
    <div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <?php if ( ! empty( $title ) ) : ?>
                        <h2 class="vil-block__title text-left title-md-center mb-30"><?php echo $title; ?></h2>
                    <?php endif; ?>

                    <div class="vil-expert mb-0">
                        <div class="d-flex vil-expert__wrap">
                            <?php while ($items_query->have_posts()) : $items_query->the_post();
                                // Setup this post for WP functions (variable must be named $post).
                                $post_id = get_the_ID();
                                $headshot = get_field('headshot', $post_id);
                                $name = get_field('name', $post_id);
                                $bio = get_field('bio', $post_id);
                                $linkedin_url = get_field('linkedin_url', $post_id);
                                $faculty_spotlight_url = get_field('faculty_spotlight_url', $post_id);
                                ?>
                                <?php if (!empty($headshot)): ?>
                                    <div class="vil-expert__photo">
                                        <img src="<?php echo esc_url($headshot['url']); ?>" alt="<?php echo $headshot['alt']; ?>">
                                    </div>
                                <?php endif ?>
                                <div class="vil-expert__content">
                                    <div class="d-flex vil-expert__head">
                                        <?php if ( ! empty( $name ) ) : ?>
                                            <h4 class="vil-expert__title"><?php echo $name; ?></h4>
                                        <?php endif ?>
                                        <?php if ( ! empty( $linkedin_url ) ): ?>
                                            <ul class="vil-expert__soc">
                                                <li>
                                                    <a href="<?php echo esc_url( $linkedin_url ); ?>" target="_blank" rel="nofollow">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M22.2857 0H1.70893C0.766071 0 0 0.776786 0 1.73036V22.2696C0 23.2232 0.766071 24 1.70893 24H22.2857C23.2286 24 24 23.2232 24 22.2696V1.73036C24 0.776786 23.2286 0 22.2857 0ZM7.25357 20.5714H3.69643V9.11786H7.25893V20.5714H7.25357ZM5.475 7.55357C4.33393 7.55357 3.4125 6.62679 3.4125 5.49107C3.4125 4.35536 4.33393 3.42857 5.475 3.42857C6.61071 3.42857 7.5375 4.35536 7.5375 5.49107C7.5375 6.63214 6.61607 7.55357 5.475 7.55357ZM20.5875 20.5714H17.0304V15C17.0304 13.6714 17.0036 11.9625 15.1821 11.9625C13.3286 11.9625 13.0446 13.4089 13.0446 14.9036V20.5714H9.4875V9.11786H12.9V10.6821H12.9482C13.425 9.78214 14.5875 8.83393 16.3179 8.83393C19.9179 8.83393 20.5875 11.2071 20.5875 14.2929V20.5714Z" fill="#2C64BC" />
                                                        </svg>
                                                    </a>
                                                </li>
                                            </ul>
                                        <?php endif ?>
                                    </div>
                                    <?php if (!empty($bio)) : ?>
                                        <div class="vil-expert__text">
                                            <p><?php echo $bio; ?></p>
                                        </div>
                                    <?php endif ?>
                                    <?php if ( ! empty( $faculty_spotlight_url ) ): ?>
                                        <a href="<?php echo esc_url( $faculty_spotlight_url ); ?>"  class="vil-expert__link">
                                            <?php esc_html_e('Read the Faculty Spotlight >', V_PREFIX); ?> 
                                        </a>
                                    <?php endif ?>
                                </div>
                            <?php endwhile; wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
