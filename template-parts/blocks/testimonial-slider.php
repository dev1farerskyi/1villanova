<?php
/*
 * Block Name: Testimonials Slider Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$items = get_field('items');
$items = ! empty( $args['items'] ) ? $args['items'] : $items;

$block_name = 'vil-reviews';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : $block_name;
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;

$args = array(
    'post_type' => 'testimonials',
    'post_status' => 'publish',
    'post_per_page' => -1,
    'post__in' => $items,
    'orderby' => 'post__in'
);
$items_query = new WP_Query($args);

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'vil-section-element';
?>
<?php if ($items_query->have_posts()) : ?>
    <div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
        <div class="container">
            <div class="vil-reviews__wrap">
                <div class="swiper swiper-reviews">
                    <div class="swiper-wrapper">
                        <?php while ($items_query->have_posts()) : $items_query->the_post();
                        // Setup this post for WP functions (variable must be named $post).
                        $post_id = get_the_ID();
                        $full_quote = get_field( 'full_quote', $post_id );
                        $name = get_field( 'name', $post_id );
                        $certificate_program__course = get_field( 'certificate_program__course', $post_id );
                        $image = get_field( 'image', $post_id );
                        ?>
                            <div class="swiper-slide">
                                <div class="vil-reviews__item">
                                    <?php if ( ! empty( $image ) ): ?>
                                        <div class="vil-reviews__item-photo">
                                            <img src="<?php echo esc_url($image['url']); ?>" alt="image">
                                        </div>
                                    <?php endif ?>
                                    <div class="vil-reviews__item-content">
                                        <div class="vil-reviews__item-icon">
                                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/assets/img/icon-review.svg" alt="icon">
                                        </div>
                                        <h5 class="vil-reviews__item-title"><?php the_title(); ?></h5>
                                        <?php if ( ! empty( $full_quote ) ) : ?>
                                            <div class="vil-reviews__item-text">
                                                <p><?php echo $full_quote; ?></p>
                                            </div>
                                        <?php endif ?>
                                        <?php if ( ! empty( $name ) ) : ?>
                                            <p class="vil-reviews__item-author"><?php echo $name; ?>
                                                <br>
                                                <?php echo $certificate_program__course; ?>
                                            </p>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </div>
                </div>

                <?php if ( $items_query->max_num_pages >= 2 ) : ?>
                    <div class="swiper-button-next swiper-reviews-next">
                        <svg width="13" height="21" viewBox="0 0 13 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.6781 9.72255L3.77699 0.821857C3.34785 0.392714 2.65169 0.392714 2.22255 0.821857L1.18427 1.86013C0.755589 2.28882 0.755131 2.98314 1.18244 3.41274L8.23695 10.4998L1.1829 17.5873C0.75513 18.0169 0.756046 18.7112 1.18473 19.1399L2.22301 20.1781C2.65215 20.6073 3.3483 20.6073 3.77745 20.1781L12.6781 11.277C13.1073 10.8478 13.1073 10.1517 12.6781 9.72255Z" fill="#727272"/>
                        </svg>
                    </div>

                    <div class="swiper-button-prev swiper-reviews-prev">
                        <svg width="13" height="21" viewBox="0 0 13 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.321857 9.72255L9.22301 0.821857C9.65215 0.392714 10.3483 0.392714 10.7774 0.821857L11.8157 1.86013C12.2444 2.28882 12.2449 2.98314 11.8176 3.41274L4.76305 10.4998L11.8171 17.5873C12.2449 18.0169 12.244 18.7112 11.8153 19.1399L10.777 20.1781C10.3478 20.6073 9.6517 20.6073 9.22255 20.1781L0.321857 11.277C-0.107286 10.8478 -0.107286 10.1517 0.321857 9.72255Z" fill="#727272"/>
                        </svg>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>