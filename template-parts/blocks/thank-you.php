<?php
/*
 * Block Name: Thank You Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$subtitle = get_field('subtitle');
$text = get_field('text');
$image = get_field('image');
$download_guide = get_field('download_guide');
$enroll_now = get_field('enroll_now');
$info = get_field('info');

if ( ! is_admin()){
    if(isset($_GET['id']) ){
        $download_guide = get_post_meta(sanitize_text_field($_GET['id']),'url_pdf',true);
    }else{
        $download_guide = ! empty( $download_guide ) ? $download_guide['url'] : '';
        global $wp_query;

        $wp_query->set_404();
        status_header(404);
    }
}

$block_name = 'vil-block';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'vil-section-element';
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <div class="container">
        <div class="vil-thank-you__main">
            <div class="row vil-thank-you__content">
                <?php if ( ! empty( $image ) ) : ?>
                    <div class="col-lg-5 col-md-5">
                        <h2 class="vil-thank-you__title-mobile"><?php echo $title; ?></h2>
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $title; ?>">
                    </div>
                <?php endif; ?>

                <div class="vil-thank-you__content-text <?php echo empty( $image ) ? 'col-12 text-center' : 'col-lg-7 col-md-7'; ?>">
                    <h1 class="vil-thank-you__title <?php echo empty( $image ) ? 'vil-thank-you__title-desktop' : ''; ?>"><?php echo $title; ?></h1>

                    <?php if ( ! empty( $subtitle ) ) : ?>
                        <h3 class="vil-thank-you__subtitle"><?php echo $subtitle; ?></h3>
                    <?php endif; ?>

                    <?php if ( ! empty( $text ) ) : ?>
                        <div class="vil-thank-you__text"><?php echo $text; ?></div>
                    <?php endif; ?>

                    <?php if ( ! empty( $download_guide ) || ! empty( $enroll_now ) ) : ?>
                        <div class="d-flex vil-hero__btns">
                            <?php if ( ! empty( $download_guide ) ) : ?>
                                <a href="<?php echo esc_url( $download_guide ); ?>" class="vil-btn vil-btn_primary" target="_blank" download="download">
                                    Download Guide
                                    <img src="<?php echo V_TEMP_URL . '/assets/img/icon.svg'; ?>" alt="">
                                </a>
                            <?php endif; ?>

                            <?php if ( ! empty( $enroll_now ) ) : ?>
                                <a href="<?php echo esc_url( $enroll_now['url'] ); ?>" class="vil-btn vil-btn_third"><?php echo esc_html( $enroll_now['title'] ); ?></a>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <?php if ( ! empty( $info ) ) : ?>
                <div class="vil-thank-you__buttons row justify-content-center">
                    <?php foreach ( $info as $item ) : ?>
                    
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="vil-thank-you__btn-contact_wrap">
                                <a href="<?php echo $item['url']; ?>" class="vil-thank-you__btn-contact">
                                    <?php if ( ! empty( $item['icon'] ) ) : ?>
                                        <span class="icon">
                                            <img src="<?php echo $item['icon']['url']; ?>" alt="<?php echo $item['title']; ?>">
                                        </span>
                                    <?php endif; ?>

                                    <p class="vil-thank-you__btn-contact-text"><?php echo $item['title']; ?></p>
                                </a>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
