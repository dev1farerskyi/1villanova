<?php
/*
 * Block Name: WYSIWYG Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$title = ! empty ( $args['title'] ) ? $args['title'] : $title;

$content = get_field('content');
$content = ! empty ( $args['content'] ) ? $args['content'] : $content;

$wysiwyg_full_container = get_field('wysiwyg_full_container');
$wysiwyg_full_container = ! empty ( $args['wysiwyg_full_container'] ) ? $args['wysiwyg_full_container'] : $wysiwyg_full_container;

$block_name = 'vil-about-program';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : '';
if ( ! empty( $block['anchor'] ) ) {
	$id = $block['anchor'];
}

$id = ! empty ( $args['id'] ) ? $args['id'] : $id;

$className = array($block_name, 'vil-section-element');

if ( ! empty( $title ) || ! empty( $content ) ): ?>
	<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-<?php echo ! empty( $wysiwyg_full_container ) ? '12' : '10'; ?>">
					<div class="vil-about-program__content">
						<?php if ( ! empty( $title ) ): ?>
							<h2><?php echo $title; ?></h2>
						<?php endif ?>

						<?php if ( ! empty( $content ) ): ?>
							<div class="vil-about-program__text"><?php echo wpautop( $content ); ?></div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>