<?php
/*
 * Block Name: Meta Detail Icon Bar Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$items = get_field('items');
$items = ! empty( $args['items'] ) ? $args['items'] : $items;

$description = get_field('description');
$description = ! empty( $args['description'] ) ? $args['description'] : $description;

$style = get_field('style');
$style = ! empty( $args['style'] ) ? $args['style'] : $style;

$block_name = 'vil-program-info';

// Create id attribute allowing for custom "anchor" value.
$id = isset( $block['id'] ) ? $block_name . '-' . $block['id'] : $block_name;
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;


// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'vil-section-element';
$className[] = 'bg_light_color';
$className[] = $style;
?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($style === 'style-5-icon-columns') : ?>
            <?php if (!empty($items)) : ?>
                <div class="vil-program-info__description-and-col">
                    <?php foreach ($items as $row):
                        $icon = $row['icon'];
                        $label = $row['label'];
                        ?>
                        <div class="vil-program-info__col">
                            <?php if (!empty($icon)): ?>
                                <div class="vil-program-info__col-icon">
                                    <img src="<?php echo esc_url($icon['url']); ?>" alt="image">
                                </div>
                            <?php endif ?>
                            <?php if (!empty($label)) : ?>
                                <?php echo $label; ?>
                            <?php endif ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif ?>
        <?php elseif ($style === 'style-4-columns') : ?>
            <?php if (!empty($items)) : ?>
                <div class="d-flex vil-program-info__wrap">
                    <?php foreach ($items as $row):
                        $icon = $row['icon'];
                        $label = $row['label'];
                        ?>
                        <div class="vil-program-info__col">
                            <?php if (!empty($icon)): ?>
                                <div class="vil-program-info__col-icon">
                                    <img src="<?php echo esc_url($icon['url']); ?>" alt="image">
                                </div>
                            <?php endif ?>
                            <?php if (!empty($label)) : ?>
                                <?php echo $label; ?>
                            <?php endif ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif ?>
        <?php elseif ($style === 'style-description-and-4-icon-columns') : ?>
            <div class="d-flex vil-program-info__wrap">
                <?php if (!empty($description)) : ?>
                    <div class="vil-program-info__description">
                        <?php echo $description; ?>
                    </div>
                <?php endif ?>
                <?php if (!empty($items)) : ?>
                    <div class="vil-program-info__description-and-col">
                        <?php foreach ($items as $row):
                            $icon = $row['icon'];
                            $label = $row['label'];
                            ?>
                            <div class="vil-program-info__col">
                                <?php if (!empty($icon)): ?>
                                    <div class="vil-program-info__col-icon">
                                        <img src="<?php echo esc_url($icon['url']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
                                    </div>
                                <?php endif ?>
                                <?php if (!empty($label)) : ?>
                                    <?php echo $label; ?>
                                <?php endif ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                <?php endif ?>
            </div>
        <?php endif ?>
    </div>
</div>
