<?php
/*
 * Block Name: Related Articles Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$style = get_field('style');
$link = get_field('link');
$display_order = get_field('display_order');
$category = get_field('category');
$posts = get_field('posts');
$featured_post = get_field('featured_post');

$block_name = 'vil-block';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : $block_name;
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;

get_template_part(
    'template-parts/elements/related-articles',
    null,
    array(
        'style' => $style,
        'title' => $title,
        'link' => $link,
        'display_order' => $display_order,
        'category' => $category,
        'posts' => $posts,
        'featured_post' => $featured_post,
        'id' => $id,
    )
);
