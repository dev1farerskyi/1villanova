<?php
/*
 * Block Name: Small image with content Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */


$items = get_field('items');
$items = ! empty ( $args['items'] ) ? $args['items'] : $items;

$block_name = 'vil-simw';
$two_items  = ! empty( $items ) && count( $items ) == 2;

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : '';
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

$id = ! empty ( $args['id'] ) ? $args['id'] : $id;


// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name, 'vil-section-element');
$className[] = $two_items ? $block_name . '-two' : $block_name . '-one';

if ( ! empty( $items ) ): ?>
    <div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="vil-simw__wrap">
                        <?php if ( $two_items ): ?>
                            <?php foreach ($items as $item): ?>
                                <div class="vil-simw__item">
                                    <div class="vil-simw__item-head">
                                        <?php if ( ! empty( $item['icon'] ) ): ?>
                                            <div class="vil-simw__item-image">
                                                <img src="<?php echo $item['icon']['url']; ?>" alt="<?php echo $item['icon']['alt'] ?>">
                                            </div>
                                        <?php endif ?>

                                        <?php if ( ! empty( $item['title'] ) ): ?>
                                            <div class="vil-simw__item-title">
                                                <?php echo $item['title']; ?>
                                            </div>
                                        <?php endif ?>
                                    </div>

                                    <?php if ( ! empty( $item['text'] ) ): ?>
                                        <div class="vil-simw__item-body">
                                            <p><?php echo $item['text']; ?></p>
                                        </div>
                                    <?php endif ?>
                                </div>
                            <?php endforeach ?>
                        <?php else: ?>
                            <?php foreach ($items as $item): ?>
                                <div class="vil-simw__item">
                                    <div class="vil-simw__item-image">
                                        <?php if ( ! empty( $item['icon'] ) ): ?>
                                            <img src="<?php echo $item['icon']['url']; ?>" alt="<?php echo $item['icon']['alt'] ?>">
                                        <?php endif ?>
                                    </div>
                                    <div class="vil-simw__item-body">
                                        <?php if ( ! empty( $item['title'] ) ): ?>
                                            <div class="vil-simw__item-title">
                                                <?php echo $item['title']; ?>
                                            </div>
                                        <?php endif ?>
                                        <?php if ( ! empty( $item['text'] ) ): ?>
                                            <?php echo $item['text']; ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif ?>