<?php
/*
 * Block Name: Nav menu
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$menu_title = get_field('menu_title');
$nav_menu   = get_field('nav_menu');
$margin_top = get_field('margin_top');

$block_name = 'vil-faq__colum';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array( $block_name, 'vil-section-element' );

if ( ! empty( $margin_top ) ) : ?>
    <style>
        <?php echo '#' . $id . '{margin-top: ' . $margin_top . 'px}'; ?>
    </style>
<?php endif; ?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <?php if ( ! empty( $menu_title ) ) : ?>
        <h4 class="vil-faq__colum-title"><?php echo $menu_title; ?></h4>
    <?php endif ?>

    <?php if ( ! empty( $nav_menu ) ) {
        wp_nav_menu(
            array(
                'container' => '',
                'items_wrap' => '<ul class="">%3$s</ul>',
                'menu' => $nav_menu->ID,
                'depth' => 1,
            )
        );
    } ?>
</div>

