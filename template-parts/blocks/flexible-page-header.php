<?php
/*
 * Block Name: Flexible Page Header Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title            = empty($args['title']) ? get_field('title') : $args['title'];
$subtitle         = get_field('subtitle');
$description      = empty($args['description']) ? get_field('description') : $args['description'];
$primary_button   = empty($args['primary_button']) ? get_field('primary_button') : $args['primary_button'];
$media_column     = empty($args['media_column']) ? get_field('media_column') : $args['media_column'];
$style            = get_field('style');
$secondary_button = empty($args['secondary_button']) ? get_field('secondary_button') : $args['secondary_button'];
$text_column_class = get_field('text_column_class');
$media_column_class = get_field('media_column_class');
$add_image_decor = get_field('add_image_decor');

$media_type = get_field('media_type');
$image      = get_field('image');
$video      = get_field('video');


$block_name = 'vil-hero';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : '';
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

get_template_part(
    'template-parts/elements/flexible-page-header', 
    null, 
    array(
        'title' => $title, 
        'subtitle' => $subtitle, 
        'description' => $description, 
        'primary_button' => $primary_button, 
        'secondary_button' => $secondary_button,
        'media_column' => $media_column,
        'media_column_class' => $media_column_class,
        'text_column_class' => $text_column_class,
        'media_type' => $media_type,
        'add_image_decor' => $add_image_decor,
        'image' => $image,
        'video' => $video,
        'class' => $style,
        'id' => $id
    )
);


