<?php
/*
 * Block Name: Full Filterable Course Listing
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$count_items = get_field('count_items');

$block_name = 'vil-courses';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name, 'vil-section-element' );
$className[] = 'bg_light_color';

get_template_part(
    'template-parts/elements/full-filterable-course-listing', 
    null, 
    array(
        'title' => $title, 
        'count_items' => $count_items,
        'class' => implode( ' ', $className ),
        'id' => $id,
    )
);
