<?php
/*
 * Block Name: Accordion Text
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$description = get_field('description');
$accordion = get_field('accordion');

$block_name = '';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name, 'vil-section-element' );

get_template_part(
    'template-parts/elements/accordion-text',
    null,
    array(
        'title' => $title,
        'description' => $description,
        'accordion' => $accordion,
        'class' => implode( ' ', $className ),
        'id' => $id,
    )
);
