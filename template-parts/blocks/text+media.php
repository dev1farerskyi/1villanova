<?php
/*
 * Block Name: Text + Media Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$title = ! empty( $args['title'] ) ? $args['title'] : $title;

$description = get_field('description');
$description = ! empty( $args['description'] ) ? $args['description'] : $description;

$primary_button = get_field('primary_button');
$primary_button = ! empty( $args['primary_button'] ) ? $args['primary_button'] : $primary_button;

$secondary_button = get_field('secondary_button');
$secondary_button = ! empty( $args['secondary_button'] ) ? $args['secondary_button'] : $secondary_button;

$media_type = get_field('media_type');
$media_type = ! empty( $args['media_type'] ) ? $args['media_type'] : $media_type;

$image = get_field('image');
$image = ! empty( $args['image'] ) ? $args['image'] : $image;

$video = get_field('video');
$alignment = get_field('alignment');

$style = get_field('style');
$style = ! empty( $args['style'] ) ? $args['style'] : $style;

$icon = get_field('icon');
$icon = ! empty( $args['icon'] ) ? $args['icon'] : $icon;

$icon_text = get_field('icon_text');
$icon_text = ! empty( $args['icon_text'] ) ? $args['icon_text'] : $icon_text;

$icon_url = get_field('icon_url');
$icon_url = ! empty( $args['icon_url'] ) ? $args['icon_url'] : $icon_url;

$block_name = $style === 'dark' ? 'vil-image-card' : 'vil-advantages';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : $block_name;
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name );
$className[] = 'vil-section-element';
$className[] = 'text-' . $alignment;
?>

<div class="<?php echo implode( ' ', $className ); ?>" id="<?php echo esc_attr( $id ); ?>">
    <?php if ( $style === 'dark') : ?>
        <div class="vil-image-card__bg"></div>
        <div class="container">
            <div class="vil-image-card__wrap">
                <div class="row flex-lg-row flex-column-reverse align-items-center">
                    <?php if ( $media_type === 'image' && !empty( $image )) : ?>
                        <div class="col-lg-6 media-col">
                            <div class="vil-image-card__image">
                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>">
                            </div>
                        </div>
                    <?php elseif ( $media_type === 'video' && !empty( $video ) ) : ?>
                        <div class="col-lg-6 media-col">
                            <div class="vil-image-card__image">
                                <?php the_field('video'); ?>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="col-lg-6 text-col">
                        <div class="vil-image-card__content">
                            <?php if ( ! empty( $title ) ) : ?>
                                <h2 class="vil-image-card__title"><?php echo $title; ?></h2>
                            <?php endif ?>
                            <?php if ( ! empty( $description ) ) : ?>
                                <div class="vil-image-card__text">
                                    <?php echo wpautop( $description ); ?>
                                </div>
                            <?php endif ?>

                            <?php if ( ! empty( $icon ) || ! empty( $icon_text ) ): ?>
                                    <div class="vil-image-card__subcontent">
                                        <?php echo ! empty( $icon_url ) ? '<a href="' . $icon_url . '">' : ''; ?>
                                            <?php if ( ! empty( $icon ) ): ?>
                                                <img src="<?php echo $icon['url'] ?>" alt="<?php echo $icon['alt'] ?>">
                                            <?php endif ?>
                                        <?php echo ! empty( $icon_url ) ? '</a>' : ''; ?>
                                        <?php if ( ! empty( $icon_text ) ): ?>
                                            <div>
                                                <p><?php echo $icon_text; ?></p>
                                            </div>
                                        <?php endif ?>
                                    </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ( $style === 'light') : ?>
        <div class="container">
            <div class="d-flex vil-advantages__wrap">
                <?php if ( $media_type === 'image' && !empty( $image )) : ?>
                    <div class="media-col">
                        <div class="vil-advantages__image">
                            <img src="<?php echo $image['sizes']['about_img']; ?>" alt="<?php echo $image['alt'] ?>">
                        </div>
                    </div>
                <?php elseif ( $media_type === 'video' && !empty( $video ) ) : ?>
                    <div class="media-col">
                        <div class="vil-advantages__image">
                            <?php the_field('video'); ?>
                        </div>
                    </div>
                <?php endif ?>

                <div class="vil-advantages__content text-col">
                    <?php if ( ! empty( $title ) ) : ?>
                        <h4 class="vil-advantages__title"><?php echo $title; ?></h4>
                    <?php endif ?>
                    <?php if ( ! empty( $description ) ) : ?>
                        <div class="vil-advantages__text <?php echo empty( $primary_button ) && empty( $secondary_button ) ? 'mb-0' : ''; ?>">
                            <?php echo wpautop( $description ); ?>
                        </div>
                    <?php endif ?>

                    <?php if ( ! empty( $primary_button ) || ! empty( $secondary_button ) ): ?>   
                        <div class="d-flex vil-advantages__btns">
                            <?php vil_get_button($primary_button, 'vil-btn vil-btn_primary vil-btn_small'); ?>
                            <?php vil_get_button($secondary_button, 'vil-btn vil-btn_third vil-btn_small'); ?>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>
