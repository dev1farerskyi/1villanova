<?php
/*
 * Block Name: Outcomes Carousel Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$slides = get_field('slides');
$slides = ! empty( $args['slides'] ) ? $args['slides'] : $slides;


$block_name = 'vil-achievements';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : $block_name;
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'vil-section-element';

?>
<?php if (!empty($slides)) : ?>
    <div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
        <div class="container">
            <div class="vil-achievements__wrap">
                <div class="swiper swiper-achievements">
                    <div class="swiper-wrapper">
                        <?php $image_counter = 0; foreach ($slides as $row): ?>
                            <div class="swiper-slide">
                                <div class="vil-achievements__slide">
                                    <div class="vil-achievements__slide-content">
                                        <?php if ( ! empty( $row['title'] ) ) : ?>
                                            <h4 class="vil-achievements__slide-title"><?php echo $row['title']; ?></h4>
                                        <?php endif ?>

                                        <?php if ( ! empty( $row['orange_promoted_text'] ) ) : ?>
                                            <p class="vil-achievements__slide-value"><?php echo $row['orange_promoted_text']; ?></p>
                                        <?php endif ?>

                                        <?php if ( ! empty( $row['description'] ) ) : ?>
                                            <div class="vil-achievements__slide-text">
                                                <?php echo wpautop( $row['description'] ); ?>
                                            </div>
                                        <?php endif ?>
                                    </div>

                                    <?php if ( ! empty( $row['image'] ) ): $image_counter++; ?>
                                        <div class="vil-achievements__slide-image">
                                            <img src="<?php echo esc_url($row['image']['url']); ?>" alt="image">
                                        </div>
                                    <?php endif ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <div class="swiper-pagination swiper-achievements-pagination <?php echo ! $image_counter ? 'swiper-achievements-pagination--center' : ''; ?>"></div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>