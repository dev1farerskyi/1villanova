<?php
/*
 * Block Name: Certificate Programs Listing
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$display_order = get_field('display_order');
$title = get_field('title');
$title = ! empty( $args['title'] ) ? $args['title'] : $title;

$description = get_field('description');

$display_order = !empty ($args['display_order']) ? $args['display_order'] : $display_order;

$items = get_field('items');
$items = !empty ($args['items']) ? $args['items'] : $items;

$anchor = !empty ($args['anchor']) ? $args['anchor'] : '';

$block_name = 'vil-programs';

// Create id attribute allowing for custom "anchor" value.
$id = !empty($block['id']) ? $block_name . '-' . $block['id'] : '';
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

if (!empty($anchor)) {
    $id = $anchor;
}


$args = array(
    'post_type' => 'certificates',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'orderby' => 'title',
    'order' => 'ASC'
);

if ($display_order == 'manual' && !empty($items)) {
    $args['post__in'] = $items;
    $args['orderby'] = 'post__in';
}

$items_query = new WP_Query($args);


// Create class attribute allowing for custom "className" and "align" values.
$className = array( $block_name, 'pb-lg-50', 'vil-section-element' );

if ($items_query->have_posts()) : ?>
    <div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
        <div class="container">
            <?php if (!empty($title) || !empty($description)) : ?>
                <div class="vil-programs__top">
                    <?php if (!empty($title)) :
                        $anchor_title = ! empty( $args['anchor_title'] ) ? 'id="' . $args['anchor_title'] . '"' : '';
                        ?>
                        <h2 <?php echo $anchor_title; ?> class="vil-courses__title mb-16"><?php echo $title; ?></h2>
                    <?php endif ?>

                    <?php if (!empty($description)): ?>
                        <p class="vil-courses__subtitle"><?php echo $description; ?></p>
                    <?php endif ?>
                </div>
            <?php endif ?>
            <div class="row">
                <?php while ($items_query->have_posts()) : $items_query->the_post(); ?>
                    <div class="col-xl-6 mb-sm-25 mb-30">
                        <?php get_template_part('template-parts/elements/single-certificate'); ?>
                    </div>
                <?php endwhile;
                wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
<?php endif; ?>