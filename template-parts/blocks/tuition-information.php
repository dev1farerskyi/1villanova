<?php
/*
 * Block Name: Tuition Information Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$tuition = get_field('tuition');
$tuition = ! empty( $args['tuition'] ) ? $args['tuition'] : $tuition;

$style = get_field('style', $tuition);
$title = get_field('title', $tuition);
$short_description = get_field('short_description', $tuition);
$supporting_text = get_field('supporting_text', $tuition);
$blue_card = get_field('blue_card', $tuition);
$cost_box = get_field('cost_box', $tuition);

$block_name = 'vil-payment';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : $block_name;
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;

$icons = array(
    '<svg width="40" height="41" viewBox="0 0 40 41" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M0 20.5001C0 9.46006 8.94 0.500061 19.98 0.500061C31.04 0.500061 40 9.46006 40 20.5001C40 31.5401 31.04 40.5001 19.98 40.5001C8.94 40.5001 0 31.5401 0 20.5001ZM4 20.5001C4 29.3401 11.16 36.5001 20 36.5001C28.84 36.5001 36 29.3401 36 20.5001C36 11.6601 28.84 4.50006 20 4.50006C11.16 4.50006 4 11.6601 4 20.5001Z" fill="#4D4D4D"/>
        <path d="M20.597 18.8234C17.3433 17.9777 16.297 17.1034 16.297 15.7417C16.297 14.1794 17.7446 13.0901 20.167 13.0901C22.2023 13.0901 23.22 13.8641 23.5926 15.0967C23.7646 15.6701 24.2376 16.1001 24.8396 16.1001H25.2696C26.2156 16.1001 26.8893 15.1684 26.5596 14.2797C25.9576 12.5884 24.553 11.1837 22.317 10.6391V9.65006C22.317 8.46039 21.3566 7.50006 20.167 7.50006C18.9773 7.50006 18.017 8.46039 18.017 9.65006V10.5961C15.2363 11.1981 13.0003 13.0041 13.0003 15.7704C13.0003 19.0814 15.738 20.7297 19.737 21.6901C23.3203 22.5501 24.037 23.8114 24.037 25.1444C24.037 26.1334 23.3346 27.7101 20.167 27.7101C17.802 27.7101 16.5836 26.8644 16.1106 25.6604C15.8956 25.1014 15.4083 24.7001 14.8206 24.7001H14.4193C13.459 24.7001 12.7853 25.6747 13.1436 26.5634C13.9606 28.5557 15.867 29.7311 18.017 30.1897V31.1501C18.017 32.3397 18.9773 33.3001 20.167 33.3001C21.3566 33.3001 22.317 32.3397 22.317 31.1501V30.2184C25.112 29.6881 27.3336 28.0684 27.3336 25.1301C27.3336 21.0594 23.8506 19.6691 20.597 18.8234Z" fill="#4D4D4D"/>
    </svg>
    ',
    '<svg width="40" height="41" viewBox="0 0 40 41" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M15.8103 16.4406C17.1119 15.139 17.1119 13.0281 15.8103 11.7265C14.5088 10.425 12.3978 10.425 11.0963 11.7265C9.79473 13.0281 9.79473 15.139 11.0963 16.4406C12.3984 17.7426 14.5088 17.7426 15.8103 16.4406ZM29.1436 25.0598C27.842 23.7582 25.7311 23.7582 24.4295 25.0598C23.128 26.3613 23.128 28.4723 24.4295 29.7738C25.7311 31.0754 27.842 31.0754 29.1436 29.7738C30.4456 28.4723 30.4456 26.3619 29.1436 25.0598ZM29.0425 13.0062L27.8644 11.8281C27.2134 11.1771 26.1582 11.1771 25.5076 11.8281L11.1978 26.1379C10.5468 26.7889 10.5468 27.8441 11.1978 28.4946L12.376 29.6728C13.027 30.3238 14.0822 30.3238 14.7327 29.6728L29.0425 15.363C29.6936 14.7124 29.6936 13.6572 29.0425 13.0062Z" fill="#00928F"/>
        <rect x="2" y="2.50037" width="36" height="35.9992" rx="17.9996" stroke="#00928F" stroke-width="4"/>
    </svg>
    ',
);

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'vil-section-element';
$className[] = $style;
?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <?php if ($style === 'simple') : ?>
            <?php if ($title): ?>
                <h2 class="vil-block__title text-left title-md-center mb-30"><?php echo $title; ?></h2>
            <?php endif; ?>

            <div class="vil-payment__box">
                <div class="d-flex vil-payment__box-wrap">
                    <div class="vil-payment__box-content">
                        <h3 class="vil-payment__box-title"><?php echo $blue_card['title']; ?></h3>
                        <h4 class="vil-payment__box-subtitle"><?php echo $blue_card['cost']; ?></h4>
                    </div>

                    <?php if ($blue_card): ?>
                        <div class="vil-payment__total">
                        <a class="vil-btn vil-btn_primary vil-btn_form mb-16" href="/enroll-now" target="_self">
			Enroll Today		</a>
                            <?php //vil_get_button($blue_card['primary_cta'], 'vil-btn vil-btn_primary vil-btn_form mb-16'); ?>
                            <?php vil_get_button($blue_card['secondary_cta'], 'vil-btn vil-btn_secondary vil-btn_form'); ?>
                        </div>
                    <?php endif; ?>
                </div>

                <?php if ( ! empty( $short_description ) ) : ?>
                    <div class="vil-payment__box-desk"><?php echo wpautop( $short_description ); ?></div>
                <?php endif; ?>
            </div>
        <?php elseif ($style === 'multiple') : ?>
            <?php if ( ! empty( $title ) ) : ?>
                <h2 class="vil-block__title text-left title-md-center mb-30"><?php echo $title; ?></h2>
            <?php endif; ?>

            <div class="vil-payment__box payment_extended">
                <div class="d-flex vil-payment__box-wrap">
                    <?php if ( ! empty( $supporting_text ) ) : ?>
                        <div class="vil-payment__box-content">
                            <h3 class="vil-payment__box-title"><?php echo $supporting_text['label']; ?></h3>
                            <p><?php echo $supporting_text['short_description']; ?></p>
                        </div>
                    <?php endif; ?>

                    <?php if ( ! empty( $cost_box ) ) : ?>
                        <div class="vil-payment__proposal">
                            <?php foreach ($cost_box as $key => $row):
                                $title = $row['title'];
                                $description = $row['description']; ?>
                                <div class="vil-payment__proposal-item">
                                    <div class="vil-payment__proposal-icon">
                                        <?php echo $icons[$key]; ?>
                                    </div>
                                    <div class="vil-payment__proposal-wrap">
                                        <?php if ( ! empty( $title ) ): ?>
                                            <h5><?php echo $title; ?></h5>
                                        <?php endif ?>
                                        <?php if ( ! empty( $description ) ): ?>
                                            <p><?php echo $description; ?></p>
                                        <?php endif ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                    <?php if ( ! empty( $blue_card ) ) : ?>
                        <div class="vil-payment__total">
                            <h5><?php echo $blue_card['title']; ?></h5>
                            <!-- <p><?php echo $blue_card['subtitle']; ?></p> -->
                            <h4><?php echo $blue_card['cost']; ?></h4>

                            <?php vil_get_button($blue_card['primary_cta'], 'vil-btn vil-btn_primary vil-btn_form mb-16'); ?>
                            <?php vil_get_button($blue_card['secondary_cta'], 'vil-btn vil-btn_secondary vil-btn_form'); ?>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="vil-payment__box-desk"><?php echo $short_description; ?></div>
            </div>
        <?php endif ?>
    </div>
</div>