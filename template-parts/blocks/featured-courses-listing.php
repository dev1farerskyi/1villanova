<?php
/*
 * Block Name: Featured Courses Listing
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$description = get_field('description');
$cta = get_field('cta');
$title_alignment = get_field('title_alignment');
$display_order 	 = get_field('display_order');
$items  = get_field('items');
$bg_light  = get_field('bg_light');

$block_name = 'vil-courses';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className   = array( $block_name, 'vil-section-element' );
$className[] = $bg_light ? 'bg_light_color pt-lg-90' : '';
$className[] = 'title-' . $title_alignment;

get_template_part(
    'template-parts/elements/featured-courses-listing', 
    null, 
    array(
        'title' => $title, 
        'description' => $description, 
        'cta' => $cta, 
        'display_order' => $display_order, 
        'items' => $items, 
        'class' => implode( ' ', $className ),
        'id' => $id,
    )
);
