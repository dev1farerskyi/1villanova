<?php
/*
 * Block Name: Form Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$style = get_field('style');
$form = get_field('form');
$text_form = get_field('text_form');
$title = get_field('title');
$description = get_field('description');
$image = get_field('image');

$block_name = 'vil-form';

// Create id attribute allowing for custom "anchor" value.
$id = $block_name . '-' . $block['id'];
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'vil-section-element';
$className[] = 'vil-form-section';
?>
<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <?php if ($style === 'style-2') : ?>
        <div class="vil-form__form-and-content">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12">
                    <div class="vil-form__left">
                        <?php if (!empty($image)): ?>
                            <img class="vil-form__left-image" src="<?php echo esc_url($image['url']); ?>" alt="icon">
                        <?php endif ?>
                        <?php if (!empty($description)) : ?>
                            <div class="vil-form__left-text">
                                <p><?php echo $description; ?></p>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12">
                    <div class="vil-form__right">
                        <?php if (!empty($form)) : ?>
                            <div class="vil-form__form">
                                <?php echo do_shortcode('[gravityform id="' . $form['id'] . '" ajax="true" title="false" description="false"]'); ?>
                                <?php if (!empty($text_form)) : ?>
                                    <div class="vil-form__form-text-bottom"><?php echo $text_form; ?></div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    <?php elseif ($style === 'style-1') : ?>
        <div class="row">
            <div class="col-12">
                <div class="vil-form__only-form">
                    <?php if (!empty($title)) : ?>
                        <h3 class="vil-form__title"><?php echo $title; ?></h3>
                    <?php endif; ?>
                    <?php if (!empty($form)) : ?>
                        <div class="vil-form__form">
                            <?php echo do_shortcode('[gravityform id="' . $form['id'] . '" ajax="true" title="false" description="false"]'); ?>
                            <?php if (!empty($text_form)) : ?>

                                <div class="vil-form__form-text-bottom"><?php echo $text_form; ?></div>

                            <?php endif; ?>
                        </div>

                    <?php endif; ?>


                </div>
            </div>
        </div>
    <?php endif ?>
</div>
