<?php
/*
 * Block Name: FAQ Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$title = ! empty ( $args['title'] ) ? $args['title'] : $title;

$items = get_field('items');
$items = ! empty ( $args['items'] ) ? $args['items'] : $items;

$style = get_field('style');
$style = ! empty ( $args['style'] ) ? $args['style'] : $style;

$anchor = ! empty ( $args['anchor'] ) ? $args['anchor'] : '';

$nav_menu = get_field('nav_menu');
$nav_menu = ! empty ( $args['nav_menu'] ) ? $args['nav_menu'] : $nav_menu;

$menu_title = get_field('menu_title');
$menu_title = ! empty ( $args['menu_title'] ) ? $args['menu_title'] : $menu_title;

$block_name = 'vil-questions';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : '';
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

if ( ! empty( $anchor ) ) {
    $id = $anchor;
}

$id = ! empty ( $args['id'] ) ? $args['id'] : $id;

$args = array(
    'post_type' => 'faq',
    'post_status' => 'publish',
    'posts_per_page' => 4
);

if ( ! empty( $items ) ) {
    $args['post__in'] = $items;
    $args['posts_per_page'] = -1;
    $args['orderby'] = 'post__in';
}

$items_query = new WP_Query($args);

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name, 'vil-section-element');
$className[] = 'vil-block';
?>
<?php if ( $items_query->have_posts() ) : ?>
    <div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
        <div class="container">
            <?php if (!empty($title)) :
                $anchor_title = ! empty( $args['anchor_title'] ) ? 'id="' . $args['anchor_title'] . '"' : '';
                ?>
                <h2 <?php echo $anchor_title; ?> class="vil-block__title"><?php echo $title; ?></h2>
            <?php endif ?>

            <div class="row justify-content-center">
                <?php while ($items_query->have_posts()) : $items_query->the_post();
                    // Setup this post for WP functions (variable must be named $post).
                    $post_id = get_the_ID();
                    $question = get_field('question', $post_id);
                    $question = ! empty( $question ) ? $question : get_the_title();
                    $answer = get_field('answer', $post_id);
                    ?>
                    <div class="col-lg-10">
                        <div class="vil-accordion">
                            <div class="vil-accordion__head"><?php echo $question; ?></div>

                            <?php if (!empty($answer)) : ?>
                                <div class="vil-accordion__body">
                                    <?php echo wpautop($answer); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endwhile;
                wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
<?php endif; ?>