<?php
/*
 * Block Name: Text + Icon Columns Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$title = get_field('title');
$title = ! empty( $args['title'] ) ? $args['title'] : $title;

$style = get_field('style');
$style = ! empty( $args['style'] ) ? $args['style'] : $style;

$description = get_field('description');
$description = ! empty( $args['description'] ) ? $args['description'] : $description;

$items = get_field('items');
$items = ! empty( $args['items'] ) ? $args['items'] : $items;


$dark_background = get_field('dark_background');
$dark_background = ! empty( $args['dark_background'] ) ? $args['dark_background'] : $dark_background;

//$block_name = 'vil-payment__method';

$block_name = 'vil-payment';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : $block_name;
if (!empty($block['anchor'])) {
    $id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'd-flex';
$className[] = 'vil-payment__method';

$wrapper = $dark_background ? 'vil-payment-options' : '';
?>

<div class="vil-payment__block vil-section-element <?php echo $style; ?>">
    <div class="container">
        <div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
            <div class="vil-payment__method-info">
                <?php if ( ! empty( $title ) ) : ?>
                    <h4 class="vil-payment__method-title"><?php echo $title; ?></h4>
                <?php endif ?>

                <?php if ( ! empty( $description ) ) : ?>
                    <div class="vil-payment__method-text">
                        <p><?php echo $description; ?></p>
                    </div>
                <?php endif ?>
            </div>

            <?php if ( ! empty( $items ) ) : ?>
                <div class="vil-payment__method-type">
                    <div class="row">
                        <?php foreach ( $items as $row ) : ?>
                            <div class="col-sm-6 mb-sm-24 mb-12">
                                <a href="<?php echo esc_url( $row['link'] ); ?>" class="vil-btn vil-payment__option">
                                    <?php if ( ! empty( $row['icon'] ) ) : ?>
                                        <span class="vil-payment__option-icon">
											<img src="<?php echo esc_url( $row['icon']['url'] ); ?>" alt="icon">
										</span>
                                    <?php endif ?>

                                    <?php if ( ! empty( $row['description'] ) ) : ?>
                                        <?php echo $row['description']; ?>
                                    <?php endif ?>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
</div>
