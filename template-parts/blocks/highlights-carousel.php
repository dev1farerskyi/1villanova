<?php
/*
 * Block Name: Highlights Carousel Block
 * Slug:
 * Description:
 * Keywords:
 * Dependency:
 * Align: false
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

$description = get_field('description');
$description = ! empty( $args['description'] ) ? $args['description'] : $description;

$title = get_field('title');
$title = ! empty( $args['title'] ) ? $args['title'] : $title;

$items = get_field('items');
$items = ! empty( $args['items'] ) ? $args['items'] : $items;

$block_name = 'vil-learn';

// Create id attribute allowing for custom "anchor" value.
$id = ! empty( $block['id'] ) ? $block_name . '-' . $block['id'] : $block_name;
if ( ! empty( $block['anchor'] ) ) {
    $id = $block['anchor'];
}

$id = ! empty( $args['id'] ) ? $args['id'] : $id;

// Create class attribute allowing for custom "className" and "align" values.
$className = array($block_name);
$className[] = 'vil-section-element';
$className[] = 'vil-block';
?>

<div class="<?php echo implode(' ', $className); ?>" id="<?php echo esc_attr($id); ?>">
    <div class="container">
        <div class="vil-learn__head">
            <div class="row justify-content-center">
                <div class="col-xl-8 col-lg-10">
                    <?php if (!empty($title)) : ?>
                        <h2 class="vil-block__title"><?php echo $title; ?></h2>
                    <?php endif ?>
                    <?php if (!empty($description)) : ?>
                        <p class="vil-block__subtitle"><?php echo $description; ?></p>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <?php if ( ! empty( $items ) ) : ?>
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="row align-items-center">
                        <div class="col-lg-5">
                            <?php foreach ($items as $index => $row):
                                $title = $row['title'];
                                $description = $row['description'];
                                ?>
                                <div class="d-flex flex-column align-items-center pr-xl-10 vil-learn__tabs">
                                    <div class="vil-learn__tab <?php echo $index === 0 ? 'active' : ''; ?>"
                                         data-tab="<?php echo 'tab' . ($index + 1); ?>">
                                        <?php if (!empty($title)) : ?>
                                            <h6><?php echo $title; ?></h6>
                                        <?php endif ?>
                                        <?php if (!empty($description)) : ?>
                                            <p><?php echo $description; ?></p>
                                        <?php endif ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="col-lg-7">
                            <?php foreach ($items as $index =>$row):
                                $media = $row['media'];
                                $youtube = $row['youtube'];
                                $image = $row['image'];
                                ?>
                                <div class="vil-learn__content pl-xl-10 <?php echo $index === 0 ? 'active' : ''; ?>"
                                     id="<?php echo 'tab' . ($index + 1); ?>">
                                    <div class="vil-learn__image">
                                        <?php if ($media === 'image') : ?>
                                            <?php if (!empty($image)): ?>
                                                <img src="<?php echo $image['url']; ?>"
                                                     alt="<?php echo $image['alt'] ?>"/>
                                            <?php endif; ?>
                                        <?php elseif ($media === 'youtube') : ?>
                                            <?php if (!empty($youtube)): ?>
                                                <?php
                                                echo $youtube; ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>