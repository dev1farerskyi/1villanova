<?php
$query_args = array(
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => 3,
);

$featured_args = array(
    'post_type' => 'post',
);

if (!empty($args['featured_post'])) {
    $featured_args['post__in'] = array($args['featured_post']);
    $query_args['post__not_in'] = array($args['featured_post']);
}

if ($args['display_order'] === 'category') {
    $query_args['tax_query'] = array(
        array(
            'taxonomy' => 'category',
            'field' => 'id',
            'terms' => $args['category'],
        ),
    );
} elseif ($args['display_order'] === 'manual') {
    $query_args['post__in'] = $args['posts'];
}

$query_posts = new WP_Query($query_args);
?>

<div class="vil-block vil-articles vil-featured-content vil-section-element" id="<?php echo esc_attr( $args['id'] ); ?>">
    <?php if ($args['style'] === '3-posts') : ?>
        <div class="container">
            <div class="vil-articles__top">
                <?php if (!empty($args['title'])) : ?>
                    <h2 class="vil-articles__title"><?php echo $args['title']; ?></h2>
                <?php endif ?>
                <?php vil_get_button($args['link'], 'vil-btn vil-btn_secondary'); ?>
            </div>

            <div class="row">
                <?php while ($query_posts->have_posts()) : $query_posts->the_post();
                    $time_to_read = get_field('time_to_read', get_the_ID() );
                    ?>
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="vil-articles__card">
                            <div class="vil-articles__card-image">
                                <img src="<?php echo get_the_post_thumbnail_url(); ?>"
                                     alt="image">
                            </div>


                            <div class="vil-articles__card-info">
                                <?php if (!empty(get_the_category())) : ?>
                                    <?php foreach ((get_the_category()) as $category): ?>
                                        <a href="<?php echo get_term_link($category); ?>" class="vil-articles__card-category" href=""><?php echo $category->name; ?></a>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <div class="vil-articles__card-wrap">
                                    <?php if (!empty($time_to_read)) : ?>
                                        <img class="vil-articles__card-clock"
                                             src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/articles-clock.svg"
                                             alt="">
                                        <p class="vil-articles__card-time-read"><?php echo $time_to_read; ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <a href="<?php the_permalink(); ?>"
                               class="vil-articles__card-title"><?php the_title(); ?></a>
                        </div>
                    </div>
                <?php endwhile;
                wp_reset_postdata(); ?>

                <div class="vil-articles__btn-mobile">
                    <?php vil_get_button($args['link'], 'vil-btn vil-btn_secondary'); ?>
                </div>
            </div>
        </div>
    <?php elseif ( $args['style'] === 'featured-3-posts' ) : ?>
        <div class="container">
            <div class="vil-featured-content__wrap">
                <div class="d-flex vil-featured-content__head">
                    <?php if (!empty($args['title'])) : ?>
                        <h2 class="vil-articles__title">
                            <?php echo $args['title']; ?>
                        </h2>
                    <?php endif ?>

                    <?php vil_get_button($args['link'], 'vil-btn vil-btn_secondary vil-btn_small d-none d-md-block'); ?>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-12 col-sm-12">
                        <?php
                        $featured_query = new WP_Query($featured_args); ?>
                        <?php if ($featured_query->have_posts()) : ?>
                            <?php while ($featured_query->have_posts()) : $featured_query->the_post();
                                $time_to_read = get_field('time_to_read', get_the_ID() );
                                ?>
                                <div class="vil-featured-content__post item_light">
                                    <div class="vil-main-post__content-image-wrap">
                                        <img class="vil-main-post__content-image"
                                             src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                                    </div>
                                    <div class="">
                                        <?php if (!empty(get_the_category())) : ?>
                                            <?php foreach ((get_the_category()) as $category): ?>
                                                <a href="<?php echo get_term_link($category); ?>" class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></a>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                        <?php if (!empty($time_to_read)) : ?>
                                            <img class="vil-articles__card-clock"
                                                 src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/articles-clock.svg"
                                                 alt="">
                                            <span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <h4 class="vil-featured-content__title-post">
                                        <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>
                                    </h4>
                                    <div class="vil-main-post__content">
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>

                    <div class="col-lg-6 col-md-12 col-sm-12 pl-xl-40">
                        <div class="vil-featured-content__three-posts">
                            <?php while ($query_posts->have_posts()) : $query_posts->the_post();
                                $time_to_read = get_field('time_to_read', get_the_ID() );
                                ?>
                                <div class="vil-featured-content__three-posts-card item_light">
                                    <div class="row">
                                        <div class="col-lg-5 col-md-5 col-sm-5">
                                            <div class="vil-featured-content__three-posts-card-image-wrap">
                                                <img class="vil-featured-content__three-posts-card-image"
                                                     src="<?php echo get_the_post_thumbnail_url(); ?>" alt="image">
                                            </div>
                                        </div>
                                        <div class="col-lg-7 col-md-7 col-sm-7">
                                            <div class="vil-featured-content__three-posts-card-content">
                                                <div class="vil-featured-content__three-posts-card-top">
                                                    <?php if (!empty(get_the_category())) : ?>
                                                        <?php foreach ((get_the_category()) as $category): ?>
                                                            <a href="<?php echo get_term_link($category); ?>" class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></a>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                    <?php if (!empty($time_to_read)) : ?>
                                                        <img class="vil-articles__card-clock"
                                                             src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/articles-clock.svg"
                                                             alt="">

                                                        <span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
                                                    <?php endif; ?>
                                                </div>
                                                <a href="<?php the_permalink(); ?>"
                                                   class="vil-featured-content__three-posts-card-title"><?php the_title(); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile;
                            wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>

                <div class="d-flex justify-content-center d-block d-md-none mt-10">
                    <?php vil_get_button($args['link'], 'vil-btn vil-btn_secondary vil-btn_small'); ?>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>

