<?php 
$class = isset( $args['class'] ) ? $args['class'] : '';

$media_column_class = '';
$text_column_class = '';

if ( $args['media_column'] === 'small' ) {
	$media_column_class = 'col-lg-6';
	$text_column_class = 'col-lg-6';
} elseif ( $args['media_column'] === 'full' ) {
	$media_column_class = 'col-lg-12';
	$text_column_class = 'col-lg-12';
} else {
	$media_column_class = 'col-lg-6';
	$text_column_class = 'col-lg-6';
}


if ( isset( $args['media_type'] ) ) {
	if ( $args['media_type'] === 'image' && empty( $args['image'] ) ) {
		$media_column_class = 'col-lg-12';
		$text_column_class = 'col-lg-12';
	}

	if ( $args['media_type'] === 'video' && empty( $args['video'] ) ) {
		$media_column_class = 'col-lg-12';
		$text_column_class = 'col-lg-12';
	}
}
?>
<div class="vil-hero vil-section-element mb-0 <?php echo $class; ?>" id="<?php echo $args['id']; ?>">
	<div class="container">
		<div class="row align-items-lg-center">
			<div class="<?php echo $text_column_class; ?>">
				<?php if ( ! empty( $args['subtitle'] ) ) : ?>
					<p class="vil-main-date mb-16"><?php echo $args['subtitle']; ?></p>
				<?php endif ?>

				<?php if ( ! empty( $args['title'] ) ) : ?>
					<h2 class="vil-main-title vil-hero__title color_light mb-0"><?php echo $args['title']; ?></h2>
				<?php endif ?>

				<?php if ( ! empty( $args['description'] ) ) : ?>
                    <div class="vil-hero__text mt-16">
                        <?php echo wpautop( $args['description'] ); ?>
                    </div>
				<?php endif ?>

                <?php if ( ! empty( $args['primary_button'] ) || ! empty( $args['secondary_button'] ) ) : ?>
                    <div class="d-flex vil-hero__btns">
                        <?php vil_get_button($args['primary_button'], 'vil-btn vil-btn_primary'); ?>
                        <?php vil_get_button($args['secondary_button'], 'vil-btn vil-btn_secondary'); ?>
                    </div>
                <?php endif ?>
			</div>
			
			<?php if ( ! empty( $args['video'] ) || ! empty( $args['image'] ) ): ?>
				<div class="<?php echo $media_column_class; ?>">
					<div class="vil-hero__media">
						<?php if ( $args['media_type'] === 'image') : ?>
							<div class="vil-hero__media-wrap">
								<?php if( !empty( $args['image'] ) ): ?>
									<img src="<?php echo $args['image']['url']; ?>" alt="<?php echo $args['image']['alt']; ?>">
									
									<?php if ( isset( $args['add_image_decor'] ) && $args['add_image_decor'] ): ?>
										<div class="vil-main-banner__product prod1">
											<div class="vil-main-banner__product-icon">
												<img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/banner-icon1.svg" alt="icon">
											</div>
											Convenient <br>Start Dates
										</div>
										<div class="vil-main-banner__product prod2">
											<div class="vil-main-banner__product-icon">
												<img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/banner-icon2.svg" alt="icon">
											</div>
											Faculty-Led<br>
											Instruction
										</div>
										<div class="vil-main-banner__product line-type prod3">
											<div class="vil-main-banner__product-icon">
												<img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/banner-icon3.svg" alt="icon">
											</div>
											Live Class Sessions
										</div>
									<?php endif ?>
								<?php endif; ?>
							</div>
						<?php elseif ( $args['media_type'] === 'video') : ?>
							<div class="vil-hero__media-wrap-video">
								<?php if( ! empty( $args['video'] ) ): ?>
									<?php echo $args['video']; ?>
								<?php endif; ?>
							</div>
						<?php endif ?>
					</div>
				</div>
			<?php endif ?>
		</div>
	</div>
</div>