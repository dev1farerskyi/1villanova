<?php
$col_size = $args['layout'] === 'category' ? '3' : '4';
$time_to_read = get_field('time_to_read', get_the_ID());
?>

<div class="col-lg-<?php echo esc_attr( $col_size ); ?> col-md-4 col-sm-6">
    <div class="vil-more-posts__card">
        <?php if ( has_post_thumbnail() ) : ?>
            <div class="vil-more-posts__card-image-wrap">
                <?php the_post_thumbnail('large', array('class' => 'vil-more-posts__card-image')); ?>
            </div>
        <?php endif; ?>

        <div>
            <div class="vil-more-posts__card-info">
                <div class="vil-more-posts__card-info-left">
                    <?php if ( ! empty( get_the_category() ) ) : ?>
                        <?php foreach (( get_the_category() ) as $category ): ?>
                            <a href="<?php echo get_term_link( $category ); ?>" class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></a>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>

                <div class="vil-more-posts__card-info-right">
                    <?php if ( ! empty( $time_to_read ) ) : ?>
                        <img class="vil-articles__card-clock"
                             src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/icon-clock-grey.svg"
                             alt="">
                        <span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <a href="<?php the_permalink(); ?>" class="vil-featured-content__three-posts-card-title"><?php the_title(); ?></a>
        </div>
    </div>
</div>