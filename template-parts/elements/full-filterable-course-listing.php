<?php

$terms = get_terms( array(
    'taxonomy'   => 'courses-cat',
    'hide_empty' => true,
) );

$query_args = array(
	'post_type' => 'courses',
	'post_status' => 'publish',
	'posts_per_page' => is_numeric( $args['count_items'] ) ? $args['count_items'] : 10,
	'orderby' => 'title',
    'order' => 'ASC'
);

$items_query = new WP_Query($query_args);

?>
<div class="<?php echo $args['class']; ?>" id="<?php echo $args['id']; ?>">
	<div class="container">
		<?php if ( ! empty( $args['title'] ) ):
            $anchor_title = ! empty( $args['anchor_title'] ) ? 'id="' . $args['anchor_title'] . '"' : '';
            ?>
			<h4 <?php echo $anchor_title; ?> class="vil-courses__title d-block d-sm-none"><?php echo $args['title']; ?></h4>
		<?php endif ?>

		<div class="select-wrap d-block d-sm-none mb-30">
			<select class="vil-search__select select-mobile select2" data-placeholder="Filter Courses" style="width: 100%">
                <option></option>
                <option value="all"><?php esc_html_e('View All', V_PREFIX); ?></option>
				<?php foreach ($terms as $term): ?>
					<option class="vil-search__select-item" value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
				<?php endforeach ?>
			</select>
		</div>

		<div class="vil-courses__filter d-none d-sm-block">
			<?php if ( ! empty( $args['title'] ) ):
                $anchor_title = ! empty( $args['anchor_title'] ) ? 'id="' . $args['anchor_title'] . '"' : '';
                ?>
				<h4 <?php echo $anchor_title; ?> class="vil-courses__filter-title"><?php echo $args['title']; ?></h4>
			<?php endif ?>

			<?php if ( ! empty( $terms ) ): ?>
				<div class="vil-courses__filter-select">
					<div class="vil-field filter_select">
						<select class="select2" data-placeholder="Filter Courses" style="width: 100%">
                            <option></option>
							<option value="all"><?php esc_html_e('View All', V_PREFIX); ?></option>
							<?php foreach ($terms as $term): ?>
								<option value="<?php echo $term->slug; ?>"><?php echo $term->name; ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
			<?php endif ?>
		</div>
		<?php if ( $items_query->have_posts() ) : ?>
			<div>
				<div class="row js-courses-wrapp">
					<?php while ($items_query->have_posts()) : $items_query->the_post();
						get_template_part('template-parts/elements/full-filterable-course-listing-single');
					endwhile; wp_reset_postdata(); ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
</div>