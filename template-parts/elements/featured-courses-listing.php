<?php

$query_args = array(
	'post_type' => 'courses',
	'post_status' => 'publish',
	'posts_per_page' => -1,
	'orderby' => 'title',
    'order' => 'ASC'
);

if ( $args['display_order'] == 'manual' && ! empty( $args['items'] ) ) {
	$query_args['post__in'] = $args['items'];
	$query_args['orderby'] = 'post__in';
}

$items_query = new WP_Query($query_args);
?>
<div class="<?php echo esc_attr( $args['class'] ); ?>" id="<?php echo esc_attr( $args['id'] ); ?>">
	<div class="container">
		<div class="vil-courses__head">
			<div class="vil-courses__head-main">
				<?php if ( ! empty( $args['title'] ) ):
                    $anchor_title = ! empty( $args['anchor_title'] ) ? 'id="' . $args['anchor_title'] . '"' : '';
                    ?>
					<h2 <?php echo $anchor_title; ?> class="vil-courses__title"><?php echo $args['title']; ?></h2>
				<?php endif ?>

				<?php if ( ! empty( $args['description'] ) ): ?>
					<p class="vil-courses__subtitle"><?php echo $args['description']; ?></p>
				<?php endif ?>
			</div>

			<?php vil_get_button($args['cta'], 'vil-btn vil-btn_third vil-btn_small d-none d-md-block'); ?>
		</div>

		<?php if ( $items_query->have_posts() ) : ?>
			<div class="row">
				<?php while ($items_query->have_posts()) : $items_query->the_post();
					get_template_part('template-parts/elements/single-course', null, array('layout' => 'default'));
				endwhile; wp_reset_postdata(); ?>
			</div>
		<?php endif; ?>
	</div>
</div>
