<?php
$class = isset( $args['class'] ) ? $args['class'] : '';
?>

<?php if ( ! empty( $args['title'] ) || ! empty( $args['description'] ) ): ?>
    <div class="vil-about-program vil-section-element" id="content">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-10">
                    <div class="vil-about-program__content">
                        <?php if ( ! empty( $args['title'] ) ): ?>
                            <h2><?php echo $args['title']; ?></h2>
                        <?php endif ?>

                        <?php if ( ! empty( $args['description'] ) ): ?>
                            <div class="vil-about-program__text"><?php echo wpautop( $args['description'] ); ?></div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;

if ( ! empty( $args['accordion'] ) ) : ?>
    <div class="vil-certificate vil-section-element">
        <?php foreach ( $args['accordion'] as $accordion ) : ?>
            <div class="container vil-certificate-accordion <?php echo $class; ?>" id="<?php echo $args['id']; ?>">
                <div class="row justify-content-center">
                    <div class="col-xl-10">
                        <div class="vil-accordion">
                            <?php if ( ! empty( $accordion['title'] ) ) : ?>
                                <div class="vil-accordion__head"><?php echo $accordion['title']; ?></div>
                            <?php endif; ?>

                            <div class="vil-accordion__body">
                                <?php if ( $accordion['description_type'] === 'slider' && ! empty( $accordion['cards'] ) ) : ?>
                                    <div class="vil-curriculum">
                                        <h6 class="vil-curriculum__title"><?php echo esc_html( $accordion['label'] ); ?></h6>

                                        <div class="vil-curriculum__wrap">
                                            <div class="swiper swiper-curriculum">
                                                <div class="swiper-wrapper">
                                                    <?php foreach ( $accordion['cards'] as $card ) : ?>
                                                        <div class="swiper-slide">
                                                        <div class="vil-curriculum__item">
                                                            <div class="vil-curriculum__item-week"><?php echo $card['label']; ?></div>
                                                            <div class="vil-curriculum__item-content">
                                                                <h5><?php echo $card['title']; ?></h5>

                                                                <?php echo wpautop( $card['wysiwyg'] ); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>

                                            <div class="swiper-scrollbar swiper-curriculum-scrollbar"></div>
                                        </div>
                                    </div>
                                <?php elseif ( $accordion['description_type'] === 'p_w' ) : ?>
                                    <?php if ( ! empty( $accordion['skills'] ) ) : ?>
                                        <div class="vil-skills <?php echo $accordion['background'] === 'blue' ? 'skills_secondary' : ''; ?>">
                                            <h6><?php esc_html_e('Skills Learned', V_PREFIX); ?></h6>

                                            <div class="d-flex vil-skills__list">
                                                <?php foreach ( $accordion['skills'] as $skill ) : ?>
                                                    <span class="vil-skills__item"><?php echo get_the_title($skill); ?></span>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php echo wpautop( $accordion['wysiwyg'] ); ?>
                                <?php elseif ( $accordion['description_type'] === 'pills' ) : ?>
                                    <div class="vil-skills <?php echo $accordion['background'] === 'blue' ? 'skills_secondary' : ''; ?>">
                                        <h6><?php esc_html_e('Skills Learned', V_PREFIX); ?></h6>

                                        <div class="d-flex vil-skills__list">
                                            <?php foreach ( $accordion['skills'] as $skill ) : ?>
                                                <span class="vil-skills__item"><?php echo get_the_title($skill); ?></span>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php elseif ( $accordion['description_type'] === 'related' && ! empty( $accordion['certificates'] ) ) : ?>
                                    <div class="vil-program-swiper">
                                        <div class="swiper swiper-programs">
                                            <div class="swiper-wrapper">
                                                <?php foreach ( $accordion['certificates'] as $certificate ) : ?>
                                                    <div class="swiper-slide">
                                                        <?php get_template_part('template-parts/elements/single-certificate', null, array('post_id' => $certificate)); ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>

                                        <div class="swiper-scrollbar swiper-programs-scrollbar"></div>
                                    </div>
                                <?php else : ?>
                                    <?php echo wpautop( $accordion['wysiwyg'] ); ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
