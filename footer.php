<?php
/**
 * Footer template.
 *
 * @package villanova
 * @since 1.0.0
 *
 */

$footer_logo = get_field('footer_logo', 'option');
$phone_number = get_field('phone_number', 'option');
$email = get_field('email', 'option');
$chat_with_us = get_field('chat_with_us', 'option');
$classroom_login = get_field('classroom_login', 'option');
$support = get_field('support', 'option');
$menu_1_title = get_field('menu_1_title', 'option');
$menu_2_title = get_field('menu_2_title', 'option');
$menu_3_title = get_field('menu_3_title', 'option');
$menu_4_title = get_field('menu_4_title', 'option');
$menu_5_title = get_field('menu_5_title', 'option');
$copyright_text = get_field('copyright_text', 'option');

?>
</main>
<footer class="vil-footer">
    <div class="container">
        <div class="d-flex vil-footer__top">
            <?php if ( ! empty( $footer_logo ) ): ?>
                <div>
                    <a href="<?php echo site_url('/'); ?>" class="vil-footer__logo" title="logo">
                        <img src="<?php echo $footer_logo['url']; ?>" alt="<?php echo $footer_logo['alt']; ?>">
                    </a>
                </div>
            <?php endif ?>
            <div class="d-flex vil-footer__btns">
                <?php vil_get_button($classroom_login, 'vil-btn vil-btn_third vil-btn_small'); ?>
                <?php vil_get_button($support, 'vil-btn vil-btn_third vil-btn_small'); ?>
            </div>
        </div>

        <div class="d-flex align-items-center vil-footer__contact-list">
            <?php if ( ! empty( $phone_number ) ): ?>
                <a href="tel:<?php echo $phone_number; ?>" class="vil-footer__contact">
						<span>
							<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M10.4375 0H4.3125C3.58789 0 3 0.587891 3 1.3125V12.6875C3 13.4121 3.58789 14 4.3125 14H10.4375C11.1621 14 11.75 13.4121 11.75 12.6875V1.3125C11.75 0.587891 11.1621 0 10.4375 0ZM7.375 13.125C6.89102 13.125 6.5 12.734 6.5 12.25C6.5 11.766 6.89102 11.375 7.375 11.375C7.85898 11.375 8.25 11.766 8.25 12.25C8.25 12.734 7.85898 13.125 7.375 13.125ZM10.4375 10.1719C10.4375 10.3523 10.2898 10.5 10.1094 10.5H4.64062C4.46016 10.5 4.3125 10.3523 4.3125 10.1719V1.64062C4.3125 1.46016 4.46016 1.3125 4.64062 1.3125H10.1094C10.2898 1.3125 10.4375 1.46016 10.4375 1.64062V10.1719Z" fill="#00205C" />
							</svg>
						</span>
                    <?php echo $phone_number; ?>
                </a>
            <?php endif ?>

            <a href="#" class="vil-footer__contact">
                <span>
                    <svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.25 5.6875C5.25 5.20434 4.85816 4.8125 4.375 4.8125H3.9375C2.9709 4.8125 2.1875 5.5959 2.1875 6.5625V7.875C2.1875 8.8416 2.9709 9.625 3.9375 9.625H4.375C4.85816 9.625 5.25 9.23316 5.25 8.75V5.6875ZM10.0625 9.625C11.0291 9.625 11.8125 8.8416 11.8125 7.875V6.5625C11.8125 5.5959 11.0291 4.8125 10.0625 4.8125H9.625C9.14184 4.8125 8.75 5.20434 8.75 5.6875V8.75C8.75 9.23316 9.14184 9.625 9.625 9.625H10.0625ZM7 0C3.09477 0 0.125234 3.24926 0 7V7.4375C0 7.67922 0.195781 7.875 0.4375 7.875H0.875C1.11672 7.875 1.3125 7.67922 1.3125 7.4375V7C1.3125 3.86395 3.86395 1.3125 7 1.3125C10.1361 1.3125 12.6875 3.86395 12.6875 7H12.6842C12.6864 7.06645 12.6875 11.5314 12.6875 11.5314C12.6875 12.1699 12.1699 12.6875 11.5314 12.6875H8.75C8.75 11.9626 8.16238 11.375 7.4375 11.375H6.5625C5.83762 11.375 5.25 11.9626 5.25 12.6875C5.25 13.4124 5.83762 14 6.5625 14H11.5314C12.8948 14 14 12.8948 14 11.5314V7C13.8748 3.24926 10.9052 0 7 0Z" fill="#00205C"/>
                    </svg>
                </span>
                <?php echo __('Schedule a Call'); ?>
            </a>

            <?php if ( ! empty( $email ) ): ?>
                <a href="mailto:<?php echo $email; ?>" class="vil-footer__contact">
						<span>
							<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M12.6875 2.25H1.3125C0.587617 2.25 0 2.83762 0 3.5625V11.4375C0 12.1624 0.587617 12.75 1.3125 12.75H12.6875C13.4124 12.75 14 12.1624 14 11.4375V3.5625C14 2.83762 13.4124 2.25 12.6875 2.25ZM12.6875 3.5625V4.67826C12.0744 5.17753 11.097 5.95388 9.00739 7.5901C8.54689 7.95232 7.6347 8.82254 7 8.81239C6.36541 8.82264 5.45292 7.95218 4.99261 7.5901C2.90336 5.95412 1.92568 5.17761 1.3125 4.67826V3.5625H12.6875ZM1.3125 11.4375V6.36245C1.93905 6.8615 2.82759 7.5618 4.1819 8.6223C4.77955 9.09275 5.82619 10.1313 7 10.125C8.16804 10.1313 9.20142 9.10781 9.81786 8.62252C11.1721 7.56204 12.0609 6.86155 12.6875 6.36247V11.4375H1.3125Z" fill="#00205C" />
							</svg>
						</span>
                    <?php echo $email; ?>
                </a>
            <?php endif ?>

            <?php if ( ! empty( $chat_with_us ) ): ?>
                <a href="#" class="vil-footer__contact js-show-chat">
						<span>
							<svg width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M12.9292 10.4876C13.5975 9.76173 13.9985 8.85102 13.9985 7.85726C13.9985 5.71441 12.1393 3.94387 9.71636 3.6278C8.95083 2.08494 7.15243 1.00012 5.05511 1.00012C2.26274 1.00012 0.000163682 2.91798 0.000163682 5.28584C0.000163682 6.27691 0.401157 7.18762 1.06948 7.91619C0.69765 8.73852 0.162991 9.37601 0.15327 9.38673C0.000163686 9.56619 -0.0435811 9.82869 0.0463387 10.0564C0.133828 10.2841 0.33797 10.4314 0.561555 10.4314C1.86175 10.4314 2.91162 9.8903 3.60425 9.39209C3.82783 9.44834 4.0587 9.49119 4.29444 9.52334C5.05754 11.0608 6.84865 12.143 8.94354 12.143C9.44903 12.143 9.93508 12.0787 10.3968 11.9608C11.0895 12.4564 12.1369 13.0001 13.4395 13.0001C13.6631 13.0001 13.8648 12.8528 13.9547 12.6251C14.0422 12.3974 14.0009 12.1349 13.8478 11.9555C13.8381 11.9474 13.301 11.3099 12.9292 10.4876ZM3.38309 8.0153L2.96752 8.31262C2.62485 8.55637 2.27489 8.74923 1.92007 8.88584C1.98569 8.75994 2.05131 8.62602 2.11449 8.48941L2.49118 7.65637L1.88848 7.00012C1.56039 6.64119 1.16669 6.05459 1.16669 5.28584C1.16669 3.65994 2.94807 2.28584 5.05511 2.28584C7.16215 2.28584 8.94354 3.65994 8.94354 5.28584C8.94354 6.91173 7.16215 8.28584 5.05511 8.28584C4.65412 8.28584 4.25313 8.23494 3.86428 8.13584L3.38309 8.0153ZM12.1102 9.57155L11.5099 10.2251L11.8866 11.0582C11.9498 11.1948 12.0154 11.3287 12.081 11.4546C11.7262 11.318 11.3762 11.1251 11.0336 10.8814L10.618 10.5841L10.1344 10.7073C9.74552 10.8064 9.34453 10.8573 8.94354 10.8573C7.63119 10.8573 6.45981 10.3189 5.7526 9.52601C8.21446 9.23673 10.1101 7.4528 10.1101 5.28584C10.1101 5.19477 10.1003 5.10637 10.0931 5.01798C11.6581 5.40637 12.832 6.53941 12.832 7.85726C12.832 8.62602 12.4383 9.21262 12.1102 9.57155Z" fill="#00205C" />
							</svg>
						</span>
                    <?php echo $chat_with_us; ?>
                </a>
            <?php endif ?>
        </div>

        <div class="d-flex vil-footer__nav">
            <div class="vil-footer__nav-col">
                <?php if ( ! empty( $menu_1_title ) ): ?>
                    <h6 class="vil-footer__nav-title"><?php echo $menu_1_title; ?></h6>
                <?php endif ?>

                <?php wp_nav_menu(
                    array(
                        'container' => '',
                        'items_wrap' => '<ul class="vil-footer__menu">%3$s</ul>',
                        'theme_location' => 'footer-menu-1',
                        'depth' => 1,
                        'fallback_cb' => '__return_empty_string',
                    )
                ); ?>
            </div>

            <div class="vil-footer__nav-col">
                <?php if ( ! empty( $menu_2_title ) ): ?>
                    <h6 class="vil-footer__nav-title"><?php echo $menu_2_title; ?></h6>
                <?php endif ?>

                <?php wp_nav_menu(
                    array(
                        'container' => '',
                        'items_wrap' => '<ul class="vil-footer__menu">%3$s</ul>',
                        'theme_location' => 'footer-menu-2',
                        'depth' => 1,
                        'fallback_cb' => '__return_empty_string',
                    )
                ); ?>
            </div>

            <div class="vil-footer__nav-col">
                <?php if ( ! empty( $menu_3_title ) ): ?>
                    <h6 class="vil-footer__nav-title"><?php echo $menu_3_title; ?></h6>
                <?php endif ?>

                <?php wp_nav_menu(
                    array(
                        'container' => '',
                        'items_wrap' => '<ul class="vil-footer__menu">%3$s</ul>',
                        'theme_location' => 'footer-menu-3',
                        'depth' => 1,
                        'fallback_cb' => '__return_empty_string',
                    )
                ); ?>
            </div>

            <div class="vil-footer__nav-col">
                <?php if ( ! empty( $menu_4_title ) ): ?>
                    <h6 class="vil-footer__nav-title"><?php echo $menu_4_title; ?></h6>
                <?php endif ?>

                <?php wp_nav_menu(
                    array(
                        'container' => '',
                        'items_wrap' => '<ul class="vil-footer__menu">%3$s</ul>',
                        'theme_location' => 'footer-menu-4',
                        'depth' => 1,
                        'fallback_cb' => '__return_empty_string',
                    )
                ); ?>
            </div>

            <div class="vil-footer__nav-col">
                <?php if ( ! empty( $menu_5_title ) ): ?>
                    <h6 class="vil-footer__nav-title"><?php echo $menu_5_title; ?></h6>
                <?php endif ?>

                <?php wp_nav_menu(
                    array(
                        'container' => '',
                        'items_wrap' => '<ul class="vil-footer__menu">%3$s</ul>',
                        'theme_location' => 'footer-menu-5',
                        'depth' => 1,
                        'fallback_cb' => '__return_empty_string',
                    )
                ); ?>
            </div>
        </div>

        <div class="vil-footer__bottom">
            <p>
                &copy; <?php echo date('Y'); ?>
                <?php if ( ! empty( $copyright_text ) ): ?>
                    <?php echo $copyright_text; ?>
                <?php endif ?>
            </p>

            <?php wp_nav_menu(
                array(
                    'container' => '',
                    'items_wrap' => '<ul class="vil-footer__bottom-menu">%3$s</ul>',
                    'theme_location' => 'copyright-menu',
                    'depth' => 1,
                    'fallback_cb' => '__return_empty_string',
                )
            ); ?>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
