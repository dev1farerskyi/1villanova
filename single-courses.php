<?php
/**
 * Course post template
 */

$show_block = get_field('show_block');
$i_show_block = get_field('i_show_block');
$tam_show_block = get_field('tam_show_block');
$slider_show_block = get_field('slider_show_block');
$wysing_show_block = get_field('wysing_show_block');
$small_image_show_block = get_field('small_image_show_block');
$accordion_show_block = get_field('accordion_show_block');
$meet_show_block = get_field('meet_show_block');
$highlights_show_block = get_field('highlights_show_block');
$tuition_show_block = get_field('tuition_show_block');
$text_icon_show_block = get_field('text_icon_show_block');
$testimonials_show_block = get_field('testimonials_show_block');
$faq_show_block = get_field('faq_show_block');

$page_header = get_field('header_options');
$page_header = $page_header['header_options'];

$icon_items  = vil_singular_icons('courses');
$description = get_field('description');
$text_and_image = get_field('text_and_image');
$text_and_image['id'] = 'text-and-media';

$wysiwyg = get_field('wysiwyg');
$wysiwyg['id'] = 'content';

$b_wysiwyg = get_field('b_wysiwyg');
$b_wysiwyg['id'] = 'text';

$small_image_with_content = get_field('small_image_with_content');
$small_image_with_content['id'] = 'image-with-content';

$accordion_text = get_field('accordion_text');
$accordion_text['id'] = 'accordion-text';

$course_curriculum = get_field('course_curriculum');
$learning_outcomes = get_field('learning_outcomes');
$related_programs  = get_field('related_programs');

$expert_faculty = get_field('expert_faculty');
$expert_faculty['id'] = 'expert-faculty';

$highlights_carousel = get_field('highlights_carousel');
$highlights_carousel['id'] = 'highlights-carousel';

$tuition_information = get_field('tuition_information');
$tuition_information['id'] = 'tuition-information';

$text_icon_columns = get_field('text_icon_columns');
$text_icon_columns['id'] = 'text-icon-columns';

$testimonials_slider = get_field('testimonials_slider');
$testimonials_slider['id'] = 'testimonials';

$faq = get_field('faq');
$faq['id'] = 'faq';

$form = get_field('form');

get_header();

if ( ! empty( $show_block ) ) {
    get_template_part(
        'template-parts/elements/flexible-page-header',
        null,
        array(
            'title' => get_the_title(),
            'subtitle' => vil_certificates_enroll($page_header),
            'description' => $page_header['short_description'],
            'primary_button' => $page_header['primary_cta'],
            'secondary_button' => $page_header['secondary_cta'],
            'image' => $page_header['image'],
            'video' => $page_header['video_url'],
            'media_type' => $page_header['featured_media'],
            'media_column' => $page_header['featured_media'] == 'none' ? 'full' : 'small',
            'id' => 'page-header',
        )
    );
}

if ( ! empty( $i_show_block ) ) {
    get_template_part(
        'template-parts/blocks/meta-detail-icon-bar',
        null,
        array(
            'items' => $icon_items,
            'description' => $description ,
            'style' => ! empty( $description ) ? 'style-description-and-4-icon-columns' : 'style-4-columns',
            'id' => 'icon-bar',
        )
    );
}

if ( ! empty( $tam_show_block ) ) {
    get_template_part( 'template-parts/blocks/text+media', null, $text_and_image );
}

if ( ! empty( $small_image_show_block ) ) {
    get_template_part( 'template-parts/blocks/small-image-with-content', null, $small_image_with_content );
}

if ( ! empty( $accordion_show_block ) ) {
    get_template_part( 'template-parts/elements/accordion-text', null, $accordion_text );
}

if ( ! empty( $meet_show_block ) ) {
    get_template_part( 'template-parts/blocks/meet-your-expert-faculty', null, $expert_faculty );
}

if ( ! empty( $wysing_show_block ) ) {
    get_template_part( 'template-parts/blocks/wysiwyg', null, $wysiwyg );
}

if ( ! empty( $wysing_show_block ) ) {
    get_template_part( 'template-parts/blocks/wysiwyg', null, $b_wysiwyg );
}

if ( ! empty( $highlights_show_block ) ) {
    get_template_part( 'template-parts/blocks/highlights-carousel', null, $highlights_carousel );
}

if ( ! empty( $tuition_show_block ) ) {
    get_template_part( 'template-parts/blocks/tuition-information', null, $tuition_information );
}

if ( ! empty( $text_icon_show_block ) ) {
    get_template_part( 'template-parts/blocks/text+icon-columns', null, $text_icon_columns );
}

if ( ! empty( $testimonials_show_block ) ) {
    get_template_part( 'template-parts/blocks/testimonial-slider', null, $testimonials_slider );
}

if ( ! empty( $faq_show_block ) ) {
    get_template_part( 'template-parts/blocks/faq', null, $faq );
 }
 
 ?>
 
<style>
  .modal {
    display: none;
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: -webkit-fill-available;
    padding: 20px;
    background-color: #fff;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
    z-index: 9999;
    max-height: 80%; /* Set the maximum height of the modal */
    overflow-y: auto; /* Make the modal scrollable */
  }

  .modal-header {
    text-align: center;
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 20px;
  }

  .form-group {
    margin-bottom: 10px;
  }

  .form-group label {
    font-weight: bold;
  }

  .form-group input,
  .form-group select {
    width: 100%;
    padding: 8px;
    border: 1px solid #ccc;
    border-radius: 4px;
  }

  .radio-group {
    display: flex;
    flex-direction: column;
  }

  .radio-option {
    margin-bottom: 5px;
  }

  .submit-button {
    background-color: #4CAF50;
    color: white;
    padding: 10px 20px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
  }

  .submit-button:hover {
    background-color: #45a049;
  }
  
  #gform_fields_2 input {
      height:unset;
  }
  #gform_fields_2 .gfield_radio {
    padding-top: 4%;
    }
</style>

 
 <div id="my-modal" class="modal" title="Modal Form">
 <button id="close" onclick = "closeModall();" style="float: right;background:unset;">x</button>
 <?php echo do_shortcode('[gravityform id="2" title="false" description="false"]'); ?>
 </div>                                   



<?php get_footer(); ?>


<script>
/**
document.addEventListener('DOMContentLoaded', function() {
    // Replace 'gform_2' with the ID of your Gravity Form
    var form = document.getElementById('gform_2');
    
    if (form) {
        form.addEventListener('submit', function(event) {
            event.preventDefault(); // Prevent the default form submission

            // Custom logic to execute before form submission (if needed)

            // Replace 'https://www.example.com/thank-you' with the URL of your thank you page
            window.location.href = '/thank-you';
        });
    }
});

**/
</script>

<script>

  // Function to open the modal
  function openModall() {
    const modal = document.getElementById('my-modal');
    modal.style.display = 'block';
  }

  // Function to close the modal
  function closeModall() {
    const modal = document.getElementById('my-modal');
    modal.style.display = 'none';
  }

  // Function to handle click on modal button
  function onModalButtonClick(event) {
    event.preventDefault();
    openModall();
  }

  document.addEventListener('DOMContentLoaded', function () {
    const links = document.querySelectorAll('.vil-btn_form');
    links.forEach(function (link) {
      link.addEventListener('click', onModalButtonClick);
    });

    
  });

</script>