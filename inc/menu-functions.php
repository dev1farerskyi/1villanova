<?php


function vil_nav_menu_link_attributes($atts, $menu_item, $args, $depth) {
    if ( 
        ( $depth == 0 && $args->theme_location != 'main-menu' && $args->theme_location != 'copyright-menu' )
        ||
        ( $depth == 1 && $args->theme_location == 'main-menu' )
         ) {
        $button_style = get_field('button_style', $menu_item);

        if ( $button_style ) {
            if ( isset( $atts['class'] ) ) {
                $atts['class'] = $atts['class'] . ' vil-btn vil-btn_gray vil-btn_xsmall';
            } else {
                $atts['class'] = 'vil-btn vil-btn_gray vil-btn_xsmall';
            }
        }
    }

    return $atts;
}
add_filter('nav_menu_link_attributes', 'vil_nav_menu_link_attributes', 10, 4);


function vil_nav_menu_css_class($classes, $menu_item, $args, $depth) {
    if ( $depth == 0 && $args->theme_location == 'main-menu' ) {
        $submenu_style = get_field('submenu_style', $menu_item);
        if ( ! empty( $submenu_style ) && $submenu_style != 'dafault' ) {
            $classes[] = 'item-' . $submenu_style;
        }
    }

    return $classes;
}
add_filter('nav_menu_css_class', 'vil_nav_menu_css_class', 10, 4);




function vil_acf_location_rules_types($choices) {
    $choices['Menu']['menu_level'] = 'Menu Depth';
    return $choices;
}
add_filter('acf/location/rule_types', 'vil_acf_location_rules_types');



function vil_acf_location_rule_values_level($choices) {
    $choices[0] = '0';
    $choices[1] = '1';
    $choices[2] = '2';

    return $choices;
}
add_filter('acf/location/rule_values/menu_level', 'vil_acf_location_rule_values_level');



function vil_acf_location_rule_match_level($match, $rule, $options, $field_group) {
    $current_screen = get_current_screen();
    if ( isset( $current_screen->base ) && $current_screen->base == 'nav-menus') {
        if ($rule['operator'] == "==") {
            $match = ($options['nav_menu_item_depth'] == $rule['value']);
        }
    }
    return $match;
}
add_filter('acf/location/rule_match/menu_level', 'vil_acf_location_rule_match_level', 10, 4);



function vil_wp_nav_menu_items( $items, $args ) {

    if ( $args->theme_location == 'main-menu' ) {
        $request_demo = get_field('request_demo', 'option');
        if ( ! empty( $request_demo ) ) {
            $link_target = ! empty( $request_demo['target'] ) ? $request_demo['target'] : '_self';
            $items .= '<li class="menu-item menu-item__yellow">
                            <a href="' . $request_demo['url'] . '" target="' . $link_target . '">
                                ' . $request_demo['title'] . '
                            </a>
                        </li>';
        }
    }


    return $items;
}
add_filter( 'wp_nav_menu_items', 'vil_wp_nav_menu_items', 10, 2 );
