<?php

/**
 * Enqueue scripts.
 */
function vil_enqueue_scripts()
{
    if (is_admin()) return false;

    wp_enqueue_style('montserrat-fonts', '//fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;0,600;0,700;1,300&display=swap');
    wp_enqueue_style('mulish-fonts', '//fonts.googleapis.com/css2?family=Mulish:wght@400;500;600;700;800&display=swap');
    wp_enqueue_style('select2', '//cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
    wp_enqueue_style('villanova-style', get_stylesheet_directory_uri() . '/assets/css/style.css');

    wp_enqueue_script('select2', '//cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js', array('jquery'), '1.0.0', true);
    wp_enqueue_script('villanova-script', get_stylesheet_directory_uri() . '/assets/js/script.js', array('jquery'), '1.0.0', true);

    wp_localize_script('villanova-script', 'vil_object', array(
        'ajax_url' => admin_url('admin-ajax.php'),
        'site_url' => site_url('/')
    ));

}

add_action('wp_enqueue_scripts', 'vil_enqueue_scripts');
