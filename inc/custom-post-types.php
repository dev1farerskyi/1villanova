<?php

function cptui_register_my_cpts() {

	/**
	 * Post Type: Webinars.
	 */

	$labels = [
		"name" => esc_html__( "Webinars", "villanova" ),
		"singular_name" => esc_html__( "Webinar", "villanova" ),
	];

	$args = [
		"label" => esc_html__( "Webinars", "villanova" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"rest_namespace" => "wp/v2",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => true,
		"rewrite" => [ "slug" => "webinar", "with_front" => false ],
		"query_var" => true,
		"menu_icon" => "dashicons-welcome-learn-more",
		"supports" => [ "title", "editor", "revisions" ],
		"show_in_graphql" => false,
	];

	register_post_type( "webinar", $args );

	/**
	 * Post Type: Testimonials.
	 */

	$labels = [
		"name" => esc_html__( "Testimonials", "villanova" ),
		"singular_name" => esc_html__( "Testimonials", "villanova" ),
	];

	$args = [
		"label" => esc_html__( "Testimonials", "villanova" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"rest_namespace" => "wp/v2",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => false,
		"rewrite" => [ "slug" => "testimonials", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title" ],
		"show_in_graphql" => false,
	];

	register_post_type( "testimonials", $args );

	/**
	 * Post Type: FAQ.
	 */

	$labels = [
		"name" => esc_html__( "FAQ", "villanova" ),
		"singular_name" => esc_html__( "FAQ", "villanova" ),
	];

	$args = [
		"label" => esc_html__( "FAQ", "villanova" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"rest_namespace" => "wp/v2",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => false,
		"rewrite" => [ "slug" => "faq", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title" ],
		"show_in_graphql" => false,
	];

	register_post_type( "faq", $args );

	/**
	 * Post Type: Guide Download.
	 */

	$labels = [
		"name" => esc_html__( "Guide Download", "villanova" ),
		"singular_name" => esc_html__( "Guide Download", "villanova" ),
	];

	$args = [
		"label" => esc_html__( "Guide Download", "villanova" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"rest_namespace" => "wp/v2",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => true,
		"rewrite" => [ "slug" => "guide_download", "with_front" => false ],
		"query_var" => true,
		"menu_icon" => "dashicons-download",
		"supports" => [ "title" ],
		"show_in_graphql" => false,
	];

	register_post_type( "guide_download", $args );

	/**
	 * Post Type: Certificate Programs.
	 */

	$labels = [
		"name" => esc_html__( "Certificate Programs", "villanova" ),
		"singular_name" => esc_html__( "Certificate Program", "villanova" ),
	];

	$args = [
		"label" => esc_html__( "Certificate Programs", "villanova" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"rest_namespace" => "wp/v2",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => false,
		"rewrite" => [ "slug" => "certificates", "with_front" => false ],
		"query_var" => true,
		"supports" => [ "title", "thumbnail" ],
		"show_in_graphql" => false,
	];

	register_post_type( "certificates", $args );

	/**
	 * Post Type: Skills.
	 */

	$labels = [
		"name" => esc_html__( "Skills", "villanova" ),
		"singular_name" => esc_html__( "Skill", "villanova" ),
	];

	$args = [
		"label" => esc_html__( "Skills", "villanova" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"rest_namespace" => "wp/v2",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => true,
		"rewrite" => [ "slug" => "skills", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title" ],
		"show_in_graphql" => false,
	];

	register_post_type( "skills", $args );

	/**
	 * Post Type: Courses.
	 */

	$labels = [
		"name" => esc_html__( "Courses", "villanova" ),
		"singular_name" => esc_html__( "Course", "villanova" ),
	];

	$args = [
		"label" => esc_html__( "Courses", "villanova" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"rest_namespace" => "wp/v2",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => false,
		"rewrite" => [ "slug" => "courses", "with_front" => false ],
		"query_var" => true,
		"supports" => [ "title", "thumbnail" ],
		"show_in_graphql" => false,
	];

	register_post_type( "courses", $args );

}

add_action( 'init', 'cptui_register_my_cpts' );


function cptui_register_my_cpts_faculty() {

	/**
	 * Post Type: Faculty.
	 */

	$labels = [
		"name" => esc_html__( "Faculty", "villanova" ),
		"singular_name" => esc_html__( "Faculty", "villanova" ),
	];

	$args = [
		"label" => esc_html__( "Faculty", "villanova" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"rest_namespace" => "wp/v2",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => false,
		"rewrite" => [ "slug" => "faculty", "with_front" => false ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail" ],
		"show_in_graphql" => false,
	];

	register_post_type( "faculty", $args );
}

add_action( 'init', 'cptui_register_my_cpts_faculty' );

function cptui_register_my_cpts_tuition() {

	/**
	 * Post Type: Tuition.
	 */

	$labels = [
		"name" => esc_html__( "Tuition", "villanova" ),
		"singular_name" => esc_html__( "Tuition", "villanova" ),
	];

	$args = [
		"label" => esc_html__( "Tuition", "villanova" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"rest_namespace" => "wp/v2",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"can_export" => false,
		"rewrite" => [ "slug" => "tuition", "with_front" => false ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail" ],
		"show_in_graphql" => false,
	];

	register_post_type( "tuition", $args );
}

add_action( 'init', 'cptui_register_my_cpts_tuition' );


function cptui_register_my_taxes_courses_cat() {

	/**
	 * Taxonomy: Categories.
	 */

	$labels = [
		"name" => esc_html__( "Categories", "villanova" ),
		"singular_name" => esc_html__( "Category", "villanova" ),
	];

	
	$args = [
		"label" => esc_html__( "Categories", "villanova" ),
		"labels" => $labels,
		"public" => false,
		"publicly_queryable" => false,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'courses-cat', 'with_front' => true, ],
		"show_admin_column" => true,
		"show_in_rest" => false,
		"show_tagcloud" => false,
		"rest_base" => "courses-cat",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"rest_namespace" => "wp/v2",
		"show_in_quick_edit" => true,
		"sort" => true,
		"show_in_graphql" => false,
	];
	register_taxonomy( "courses-cat", [ "courses" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_courses_cat' );