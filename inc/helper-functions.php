<?php

/**
 * ACF Options page support
 */
if ( function_exists('acf_add_options_page') ) {
	acf_add_options_page();	
}

/**
 * Get button
 *
 * @param $button
 * @param $classes
 * @return void
 */
function vil_get_button($button, $classes = '') {
	if ( ! empty( $button ) ) :
		$link_target = ! empty( $button['target'] ) ? $button['target'] : '_self';
		?>
		<a class="<?php echo $classes; ?>" href="<?php echo $button['url']; ?>" target="<?php echo esc_attr( $link_target ); ?>">
			<?php echo $button['title']; ?>
		</a>
	<?php endif;
}

/**
 * SVG support
 */
function vil_upload_mimes($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'vil_upload_mimes');



/**
 * Courses filter
 */
function vil_get_courses() {
    $query_args = array(
        'post_type' => 'courses',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'title',
        'order' => 'ASC'
    );

    if ( ! empty( $_GET['term'] ) && $_GET['term'] !== 'all' ) {
        $query_args['tax_query'] = array(
            array(
                'taxonomy' => 'courses-cat',
                'field'    => 'slug',
                'terms'    => $_GET['term'],
            ),
        );
    }

    $items_query = new WP_Query($query_args);

    ob_start();

    if ( $items_query->have_posts() ) {
        while ($items_query->have_posts()) {
            $items_query->the_post();
            get_template_part('template-parts/elements/full-filterable-course-listing-single');
        }
        wp_reset_postdata();
    } else {
        echo sprintf('<p>%s</p>', esc_html__('Sorry, no courses matched your criteria.', V_PREFIX));
    }

    echo ob_get_clean();

    wp_die();
}
add_action( 'wp_ajax_vil_get_courses', 'vil_get_courses' );
add_action( 'wp_ajax_nopriv_vil_get_courses', 'vil_get_courses' );



// Disable Gutenberg editor for posts
function vil_use_block_editor_for_post_type($current_status, $post_type) {
	// Use your post type key instead of 'product'
	if ($post_type === 'post' || $post_type === 'faculty' || $post_type === 'tuition' ) {
		return false;
	} 

	return $current_status;
}
add_filter('use_block_editor_for_post_type', 'vil_use_block_editor_for_post_type', 10, 2);


function vil_certificates_enroll($page_header) {
	$day = date('d');
	$year = date('Y');
	$month = date('m');
	$next_year = false;

	if ( $page_header['date'] == 'dynamic' ) {
		$days = date('t');
		$days_diff = $days - $day;

		if ( $days_diff <= 5 ) {
			$new_month = $month + 2 > 12 ? $month + 2 - 12 : $month + 2;
			$next_year = $new_month <= 2;
		} else {
			$new_month = $month + 1 > 12 ? $month + 1 - 12 : $month + 1;
			$next_year = $new_month <= 1;
		}

		$year = $next_year ? $year + 1 : $year;
		$new_month = $new_month - 1;

		return 'Enroll by ' . date("F", strtotime("January +$new_month months")) . ' 1, ' . $year;
	} else {
		if ( $page_header['manual_date_options'] == 'on-demand' ) {
			if ( ! empty( $page_header['choose_date'] ) ) {
				return 'Enroll by ' . $page_header['choose_date'];
			}
		}

		if ( $page_header['manual_date_options'] == 'every-month' ) {
			return 'Enroll by ' . date('F j, Y', strtotime(date('m', strtotime('+1 month')).'/01/'.date('Y').' 00:00:00'));
		}

		if ( $page_header['manual_date_options'] == 'every-odd-month' ) {
			// не парний
			$new_month = ( $month + 1 ) % 2 == 0 ? $month + 2 : $month + 1;
			$next_year = $new_month > 12;
			$new_month = $new_month > 12 ? $new_month - 12 : $new_month;

			$year = $next_year ? $year + 1 : $year;
			$new_month = $new_month - 1;

			return 'Enroll by ' . date("F", strtotime("January +$new_month months")) . ' 1, ' . $year;
		}
			
		if ( $page_header['manual_date_options'] == 'every-even-month' ) {
			$new_month = ( $month + 1 ) % 2 == 0 ? $month + 1 : $month + 2;
			$next_year = $new_month > 12;
			$new_month = $new_month > 12 ? $new_month - 12 : $new_month;

			$year = $next_year ? $year + 1 : $year;
			$new_month = $new_month - 1;

			return 'Enroll by ' . date("F", strtotime("January +$new_month months")) . ' 1, ' . $year;
		}
			
		if ( $page_header['manual_date_options'] == 'quarterly' ) {
			$month = $month - 1;
			$new_month = intval(($month + 3) / 3) * 3 + 1;
			$next_year = $new_month > 12;
			$new_month = $new_month > 12 ? $new_month - 12 : $new_month;

			$year = $next_year ? $year + 1 : $year;
			$new_month = $new_month - 1;

			return 'Enroll by ' . date("F", strtotime("January +$new_month months")) . ' 1, ' . $year;
		}
	}
}


function vil_singular_icons($type = 'certificate') {
	$list = array();
	if ( $type == 'certificate' ) {
		$duration = get_field('i_duration');
		$duration_icon = get_field('i_duration_iocn');
		$courses = get_field('i_courses');
		$courses_icon = get_field('i_courses_icon');
		$starting_price = get_field('i_starting_price');
		$starting_price_icon = get_field('i_starting_price_icon');
		$payment_type = get_field('i_payment_type');
		$payment_type_icon = get_field('i_payment_type_icon');

		$additional_column = get_field('i_additional_column');
	}

	if ( $type == 'courses' ) {
		$duration = get_field('duration');
		$duration_icon = get_field('duration_icon');
		$courses = get_field('courses');
		$courses_icon = get_field('courses_icon');
		$starting_price = get_field('starting_price');
		$starting_price_icon = get_field('starting_price_icon');
		$payment_type = get_field('payment_type');
		$payment_type_icon = get_field('payment_type_icon');

		$additional_column = get_field('additional_column');
	}

	$icons_url = get_stylesheet_directory_uri() . '/assets/img/icons/';
	
	if ( ! empty( $duration ) ) {
		$list[] = array(
			'label' => $duration,
			'icon' => array(
				'url' => ! empty( $duration_icon ) ? $duration_icon['url'] : $icons_url . 'weeks.svg',
				'alt' => '',
			)
		);
	}

	if ( ! empty( $courses ) ) {
		$list[] = array(
			'label' => $courses,
			'icon' => array(
				'url' => ! empty( $courses_icon ) ? $courses_icon['url'] : $icons_url . 'courses.svg',
				'alt' => '',
			)
		);
	}

	if ( ! empty( $starting_price ) ) {
		$list[] = array(
			'label' => $starting_price,
			'icon' => array(
				'url' => ! empty( $starting_price_icon ) ? $starting_price_icon['url'] : $icons_url . 'starting_price.svg',
				'alt' => '',
			)
		);
	}

	if ( ! empty( $payment_type ) ) {
		$list[] = array(
			'label' => $payment_type,
			'icon' => array(
				'url' => ! empty( $payment_type_icon ) ? $payment_type_icon['url'] : $icons_url . 'payments.svg',
				'alt' => '',
			)
		);
	}
	
	if ( ! empty( $additional_column['icon'] ) && ! empty( $additional_column['text'] ) ) {
		$list[] = array(
			'label' => $additional_column['text'],
			'icon' => $additional_column['icon']
		);
	}

	return $list;
}


function vil_category_filter( $query ){
    if( $query->is_main_query() && ! is_admin() ) {

        if ( is_category() ) {
            if ( isset( $_GET['order'] ) ) {
                if ( $_GET['order'] == 'read-time' ) {
                    $query->set( 'meta_query', array(
                        array(
                            'key' => 'time_to_read',
                            'type' => 'NUMERIC'
                        )
                    ));

                    $query->set( 'order' , 'DESC' );
                }

                if ( $_GET['order'] == 'asc' ) {
                    $query->set( 'orderby', 'date' );
                    $query->set( 'order', 'ASC' );
                }
            }
        } elseif ( is_search() ) {
            $search_query = get_search_query();
            $paged = get_query_var('paged') ? get_query_var('paged') : 1;

            $query->set( 's', $search_query );
            $query->set( 'post_type',  array( 'post', 'program', 'course', 'video' ) );
            $query->set( 'orderby', 'relevance' );
            $query->set( 'posts_per_page', 10 );
            $query->set( 'paged', $paged );

            // Check if filter parameter is set
            if ( isset( $_GET['post_type'] ) ) {
                $filter = sanitize_text_field( $_GET['post_type'] );

                if ( $filter == 'any' ) {
                    $filter = array( 'post', 'certificates', 'courses', 'video' );
                }

                $query->set( 'post_type', $filter );
            } else {
                $filter = array( 'post', 'certificates', 'courses', 'video' );

                $query->set( 'post_type', $filter );
            }

            // Check if sort parameter is set
            if ( isset( $_GET['sort'] ) ) {
                $sort = sanitize_text_field( $_GET['sort'] );

                switch( $sort ) {
                    case 'newest':
                        $query->set( 'orderby', 'date' );
                        $query->set( 'order', 'DESC' );
                        break;
                    case 'oldest':
                        $query->set( 'orderby', 'date' );
                        $query->set( 'order', 'ASC' );
                        break;
                    case 'az':
                        $query->set( 'orderby', 'title' );
                        $query->set( 'order', 'ASC' );
                        break;
                }
            }
        }
    }
}
add_action( 'pre_get_posts', 'vil_category_filter' );


function vil_load_posts() {
	$post = $_POST;
	$big  = 999999999;
	$args = array(
		'post_type' => 'post',
        'post_status' => 'publish',
//        'posts_per_page' => get_option( 'posts_per_page' ),
		'paged' => $post['args']['page']
	);

	if ( $post['args']['category'] != 'all' ) {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'category',
				'field'    => 'slug',
				'terms'    => $post['args']['category'],
			),
		);
	}

	$the_query = new WP_Query( $args );

    $layout = ! empty( $post['args']['layout'] ) ? $post['args']['layout'] : 'blog';

	if ( $the_query->have_posts() ) { 
		echo '<div class="row">';
		while ( $the_query->have_posts() ) : $the_query->the_post(); 
			get_template_part('template-parts/elements/single-post', null, array('layout' => $layout));
		endwhile; 
		echo '</div>';

		echo '<div class="vil-more-posts__pagination">';
			
            echo paginate_links(
                array(
                    'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                    'format' => '?paged=%#%',
                    'prev_next' => false,
                    'current' => max(
                        1,
                        $post['args']['page']
                    ),
                    'total' => $the_query->max_num_pages
                )
            );
		echo '</div>';

		wp_reset_postdata(); 

	} else {
		echo 'Sorry, no posts matched your criteria.';
	} 

	wp_die();
}
add_action( 'wp_ajax_vil_load_posts', 'vil_load_posts' ); 
add_action( 'wp_ajax_nopriv_vil_load_posts', 'vil_load_posts' );

/**
 * Footer oembed code
 *
 * @return void
 */
function vil_footer_oembed_code() {
	$footer_oembed_code = get_field('footer_oembed_code', 'option');

	if ( ! empty( $footer_oembed_code ) ) {
		echo $footer_oembed_code;
	}
}
add_action('wp_footer', 'vil_footer_oembed_code');

function sort_posts()
{
	$paged = $_POST['page'];
	$sortby = $_POST['sortby'];
	$category = $_POST['category'];

	$args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => 16,
		'paged' => $paged,
		'order' => 'ASC',
		'orderby' => 'date',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field' => 'id',
				'terms' => $category,
			),
		),
	);

	if ($sortby === 'newest') {
		$args['order'] = 'DESC';
	}

	if ($sortby === 'quick-reads') {
		$args['orderby'] = 'meta_value_num';
		$args['meta_key'] = 'time_to_read';
	}

	$wp_query = new WP_Query($args);
	ob_start();
	if ($wp_query->have_posts()) :
		while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
			<?php $time_to_read = get_field('time_to_read', get_the_ID()); ?>
			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="vil-more-posts__card">
					<?php if ( has_post_thumbnail() ) : ?>
						<div class="vil-more-posts__card-image-wrap">
							<?php the_post_thumbnail('large', array('class' => 'vil-more-posts__card-image')); ?>
						</div>
					<?php endif; ?>

					<div class="content">
						<div class="vil-more-posts__card-info">
							<div class="vil-more-posts__card-info-left">
								<?php if (!empty(get_the_category())) : ?>
									<?php foreach ((get_the_category()) as $category): ?>
										<div class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></div>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>
							<div class="vil-more-posts__card-info-right">
								<?php if (!empty($time_to_read)) : ?>
									<img class="vil-articles__card-clock"
										 src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/icon-clock-grey.svg"
										 alt="">
									<span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
								<?php endif; ?>
							</div>
						</div>
						<a href="<?php the_permalink(); ?>"
						   class="vil-featured-content__three-posts-card-title"><?php the_title(); ?></a>
					</div>
				</div>
			</div>
		<?php endwhile;
	endif;
	$content = ob_get_clean();
	wp_reset_query();

	$max_pages = $wp_query->max_num_pages;

	$result = ['content' => $content, 'max_pages' => $max_pages];
	wp_send_json($result);
	wp_die();
}

add_action('wp_ajax_sort_posts', 'sort_posts');
add_action('wp_ajax_nopriv_sort_posts', 'sort_posts');

function load_posts()
{
	$paged = $_POST['page'];
	$sortby = $_POST['sortby'];
	$category = $_POST['category'];

	$args = array(
		'post_type' => 'post',
		'post_status' => 'publish',
		'posts_per_page' => 16,
		'paged' => $paged,
		'order' => 'ASC',
		'orderby' => 'date',
		'tax_query' => array(
			array(
				'taxonomy' => 'category',
				'field' => 'id',
				'terms' => $category,
			),
		),
	);

	if ($sortby === 'newest') {
		$args['order'] = 'DESC';
	}

	if ($sortby === 'quick-reads') {
		$args['orderby'] = 'meta_value_num';
		$args['meta_key'] = 'time_to_read';
	}

	$wp_query = new WP_Query($args);
	ob_start();
	if ($wp_query->have_posts()) :
		while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
			<?php $time_to_read = get_field('time_to_read', get_the_ID()); ?>
			<div class="col-lg-3 col-md-4 col-sm-6">
				<div class="vil-more-posts__card">
					<?php if ( has_post_thumbnail() ) : ?>
						<div class="vil-more-posts__card-image-wrap">
							<?php the_post_thumbnail('large', array('class' => 'vil-more-posts__card-image')); ?>
						</div>
					<?php endif; ?>

					<div class="content">
						<div class="vil-more-posts__card-info">
							<div class="vil-more-posts__card-info-left">
								<?php if (!empty(get_the_category())) : ?>
									<?php foreach ((get_the_category()) as $category): ?>
										<div class="vil-main-post__sidebar-articles-category"><?php echo $category->name; ?></div>
									<?php endforeach; ?>
								<?php endif; ?>
							</div>
							<div class="vil-more-posts__card-info-right">
								<?php if (!empty($time_to_read)) : ?>
									<img class="vil-articles__card-clock"
										 src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/icon-clock-grey.svg"
										 alt="">
									<span class="vil-main-post__content-time-read"><?php echo $time_to_read; ?></span>
								<?php endif; ?>
							</div>
						</div>
						<a href="<?php the_permalink(); ?>"
						   class="vil-featured-content__three-posts-card-title"><?php the_title(); ?></a>
					</div>
				</div>
			</div>
		<?php endwhile;
	endif;
	$content = ob_get_clean();
	wp_reset_query();

	$max_pages = $wp_query->max_num_pages;

	$result = ['content' => $content, 'max_pages' => $max_pages];
	wp_send_json($result);
	wp_die();
}

add_action('wp_ajax_load_posts', 'load_posts');
add_action('wp_ajax_nopriv_load_posts', 'load_posts');

/**
 * Show/Hide block
 *
 * @param $block_content
 * @param $block
 * @return false|mixed
 */
function vil_show_hide_block($block_content, $block) {
    if ( ! empty( $block['attrs']['data']['show_hide_block'] ) ) {
        return false;
    }

    return $block_content;
}
add_filter('render_block', 'vil_show_hide_block', 10, 2);
