<?php
/**
 * Certificate post template
 */

$show_block = get_field('show_block');
$i_show_block = get_field('i_show_block');
$tam_show_block = get_field('tam_show_block');
$slider_show_block = get_field('slider_show_block');
$wysing_show_block = get_field('wysing_show_block');
$small_image_show_block = get_field('small_image_show_block');
$accordion_show_block = get_field('accordion_show_block');
$meet_show_block = get_field('meet_show_block');
$text_icon_show_block = get_field('text_icon_show_block');
$featured_show_block = get_field('featured_show_block');
$tuition_show_block = get_field('tuition_show_block');
$testimonials_show_block = get_field('testimonials_show_block');
$elective_show_block = get_field('elective_show_block');
$related_show_block = get_field('related_show_block');
$faq_show_block = get_field('faq_show_block');

$page_header = get_field('header_options');

// Icons options
$icon_items  = vil_singular_icons();
$short_description  = get_field('i_short_description');

$text_and_image = get_field('text_and_image');

$slider_items   = get_field('slider_items');
$slider_items['id'] = 'outcomes-carousel';

$wysiwyg = get_field('wysiwyg');
$wysiwyg['id'] = 'content';

$small_image_with_content = get_field('small_image_with_content');
$small_image_with_content['id'] = 'image-with-content';

$accordion_text = get_field('accordion_text');
$accordion_text['id'] = 'accordion-text';

$meet_your_expert_faculty = get_field('meet_your_expert_faculty');
$meet_your_expert_faculty['id'] = 'expert-faculty';

$text_icon_columns = get_field('text_icon_columns');
$text_icon_columns['id'] = 'icon-columns';

$tuition_information = get_field('tuition_information');
$tuition_information['id'] = 'tuition-information';

$testimonial_slider = get_field('testimonial_slider');
$testimonial_slider['id'] = 'testimonials';

$related_articles = get_field('related_articles');
$related_articles['id'] = 'articles';

$featured_courses_listing = get_field('featured_courses_listing');
$elective_options = get_field('elective_options');

$faq = get_field('faq');
$faq['id'] = 'faq';

$skills = get_field('skills');
$certification = get_field('certification');

get_header();

if ( ! empty( $show_block ) ) {
    get_template_part(
        'template-parts/elements/flexible-page-header',
        null,
        array(
            'title' => get_the_title(),
            'subtitle' => vil_certificates_enroll($page_header),
            'description' => $page_header['short_description'],
            'primary_button' => $page_header['primary_cta'],
            'secondary_button' => $page_header['secondary_cta'],
            'image' => $page_header['image'],
            'video' => $page_header['video_url'],
            'media_type' => $page_header['featured_media'],
            'media_column' => $page_header['featured_media'] == 'none' ? 'full' : 'small',
            'id' => 'page-header',
        )
    );
}

if ( ! empty( $i_show_block ) ) {
    get_template_part(
        'template-parts/blocks/meta-detail-icon-bar',
        null,
        array(
            'items' => $icon_items,
            'description' => $short_description ,
            'style' => ! empty( $short_description ) ? 'style-description-and-4-icon-columns' : 'style-4-columns',
            'id' => 'icon-bar',
        )
    );
}

if ( ! empty( $tam_show_block ) ) {
    get_template_part(
        'template-parts/blocks/text+media',
        null,
        array(
            'title' => $text_and_image['title'],
            'description' => $text_and_image['description'],
            'primary_button' => $text_and_image['primary_button'],
            'secondary_button' => $text_and_image['secondary_button'],
            'image' => $text_and_image['image'],
            'media_type' => 'image',
            'style' => 'light',
            'id' => 'text-and-media',
        )
    );
}

if ( ! empty( $slider_show_block ) ) {
    get_template_part( 'template-parts/blocks/outcomes-carousel', null, $slider_items );
}

if ( ! empty( $accordion_show_block ) ) {
    get_template_part( 'template-parts/elements/accordion-text', null, $accordion_text );
}

if ( ! empty( $wysing_show_block ) ) {
    get_template_part( 'template-parts/blocks/wysiwyg', null, $wysiwyg );
}

if ( ! empty( $small_image_show_block ) ) {
    get_template_part( 'template-parts/blocks/small-image-with-content', null, $small_image_with_content );
}

if ( ! empty( $meet_show_block ) ) {
    get_template_part( 'template-parts/blocks/meet-your-expert-faculty', null, $meet_your_expert_faculty );
}

if ( ! empty( $featured_show_block ) ) : ?>
    <div class="vil-block vil-required bg_light_color pb-0 vil-section-element">
        <div class="container">
            <?php if ( ! empty( $featured_courses_listing['title'] ) ): ?>
                <h2 class="vil-required__title"><?php echo $featured_courses_listing['title']; ?></h2>
            <?php endif ?>

            <?php if ( ! empty( $featured_courses_listing['subtitle'] ) ): ?>
                <p class="vil-required__subtitle"><?php echo $featured_courses_listing['subtitle']; ?></p>
            <?php endif ?>

            <?php if ( ! empty( $featured_courses_listing['courses'] ) ) :
                $posts_per_page = ! empty( $featured_courses_listing['toggle_elective'] ) ? 2 : 3;

                $query_args = array(
                    'post_type' => 'courses',
                    'post_status' => 'publish',
                    'posts_per_page' => $posts_per_page,
                    'orderby' => 'post__in',
                    'order' => 'ASC',
                    'post__in' => $featured_courses_listing['courses']
                );

                $items_query = new WP_Query( $query_args );

                if ( $items_query->have_posts() ) : ?>
                    <div class="row mb-lg-10">
                        <?php while ($items_query->have_posts()) : $items_query->the_post();
                            get_template_part('template-parts/elements/single-course', null, array('layout' => 'default'));
                        endwhile; wp_reset_postdata();

                        if ( ! empty( $featured_courses_listing['toggle_elective'] ) ) {
                            get_template_part('template-parts/elements/single-course', null, array('layout' => 'elective', 'toggle_elective' => $featured_courses_listing['toggle_elective']));
                        }
                        ?>
                    </div>
                <?php endif; ?>
            <?php endif;

            if ( ! empty( $featured_courses_listing['toggle_elective'] ) ) {
                $query_args = array(
                    'post_type' => 'courses',
                    'post_status' => 'publish',
                    'posts_per_page' => -1,
                    'order' => 'ASC',
                    'orderby' => 'post__in',
                    'post__in' => $featured_courses_listing['elective_courses']
                );

                $items_query = new WP_Query( $query_args );

                if ( $items_query->have_posts() ) : ?>
                    <div id='elective-accordion' class="vil-accordion custom_accordion vil-elective_options">
                        <div class="vil-accordion__head"><?php esc_html_e('Elective Options', V_PREFIX); ?></div>
                        <div class="vil-accordion__body">
                            <div class="row">
                                <?php while ($items_query->have_posts()) : $items_query->the_post();
                                    get_template_part('template-parts/elements/full-filterable-course-listing-single');
                                endwhile; wp_reset_postdata(); ?>
                            </div>
                        </div>
                    </div>
                <?php endif;
            } ?>
        </div>
    </div>
<?php endif;

if ( ! empty( $tuition_show_block ) ) {
    get_template_part( 'template-parts/blocks/tuition-information', null, $tuition_information );
}

if ( ! empty( $text_icon_show_block ) ) {
    get_template_part( 'template-parts/blocks/text+icon-columns', null, $text_icon_columns );
}

if ( ! empty( $testimonials_show_block ) ) {
    get_template_part( 'template-parts/blocks/testimonial-slider', null, $testimonial_slider );
}

if ( ! empty( $related_show_block ) ) {
    get_template_part( 'template-parts/elements/related-articles', null, $related_articles );
}

if ( ! empty( $faq_show_block ) ) {
    get_template_part( 'template-parts/blocks/faq', null, $faq );
}

get_footer();