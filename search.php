<?php
/**
 * Search template
 */

if ( ! isset( $_GET['post_type'] ) ) {
    $search_link_with_query = site_url() . '/?s=' . $_GET['s'];
} else {
    $search_link_with_query = site_url() . '/?s=' . $_GET['s'] . '&post_type=' . $_GET['post_type'];
}

$any = 'types';
$post = 'types';
$program = 'types';
$course = 'types';
$video = 'types';

if ( isset( $_GET['post_type'] ) ) {
    $selected_post_type = $_GET['post_type'];

    if($selected_post_type == 'any'){
        $any = 'types-selected';
    }

    if($selected_post_type == 'post'){
        $post = 'types-selected';
    }

    if($selected_post_type == 'certificates'){
        $program = 'types-selected';

    }

    if($selected_post_type == 'courses'){
        $course = 'types-selected';
    }

    if($selected_post_type == 'video'){
        $video = 'types-selected';
    }
}

get_header();
?>

    <div class="vil-hero min_hero vil-section-element">
        <div class="container">
            <h1 class="vil-main-title color_light mb-16 min_heroTitle">
                <?php echo sprintf('%s <span>%s<span></span></span>', 'Search results for', get_search_query() ) ?>
            </h1>
        </div>
    </div>

    <div class="vil-block vil-section-element">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 col-lg-9">
                    <div class="vil-search__category-wrap">
                        <div class="vil-search__category">
                            <a href="<?php echo esc_url(add_query_arg('post_type', 'any', $search_link_with_query)); ?>" class="vil-search__category-link <?php echo empty($_GET['post_type']) || $_GET['post_type'] === 'any' ? 'active' : ''; ?>"><?php esc_html_e('All Results', V_PREFIX); ?></a>
                            <a href="<?php echo esc_url(add_query_arg('post_type', 'post', $search_link_with_query)); ?>" class="vil-search__category-link <?php echo (!empty($_GET['post_type']) && $_GET['post_type'] === 'post') ? 'active' : ''; ?>"><?php esc_html_e('Articles', V_PREFIX); ?></a>
                            <a href="<?php echo esc_url(add_query_arg('post_type', 'certificates', $search_link_with_query)); ?>" class="vil-search__category-link <?php echo (!empty($_GET['post_type']) && $_GET['post_type'] === 'program') ? 'active' : ''; ?>"><?php esc_html_e('Programs', V_PREFIX); ?></a>
                            <a href="<?php echo esc_url(add_query_arg('post_type', 'courses', $search_link_with_query)); ?>" class="vil-search__category-link <?php echo (!empty($_GET['post_type']) && $_GET['post_type'] === 'course') ? 'active' : ''; ?>"><?php esc_html_e('Courses', V_PREFIX); ?></a>
                            <a href="<?php echo esc_url(add_query_arg('post_type', 'video', $search_link_with_query)); ?>" class="vil-search__category-link <?php echo (!empty($_GET['post_type']) && $_GET['post_type'] === 'video') ? 'active' : ''; ?>"><?php esc_html_e('Videos', V_PREFIX); ?></a>
                        </div>
                    </div>
                </div>

                <div class="col-xl-2 col-lg-3">
                    <div class="vil-search__select d-none d-lg-block">
                        <div class="select-wrap">
                            <select class="vil-search__select select2" onchange="sort_search(this.value)" name="orderby" id="orderby" style="width: 100%">
                                <option selected disabled><?php esc_html_e('Sort Results', V_PREFIX); ?></option>
                                <option class="vil-search__select-item" value="relevance"><?php esc_html_e( 'Relevance', V_PREFIX ); ?></option>
                                <option class="vil-search__select-item" value="newest"><?php esc_html_e( 'Newest', V_PREFIX ); ?></option>
                                <option class="vil-search__select-item" value="oldest"><?php esc_html_e( 'Oldest', V_PREFIX ); ?></option>
                                <option class="vil-search__select-item" value="az"><?php esc_html_e( 'A-Z', V_PREFIX ); ?></option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-xl-10 col-lg-9">
                    <?php if ( have_posts() ) : ?>
                        <div class="vil-search-list">
                            <?php while ( have_posts() ) : the_post(); ?>
                                <div class="vil-search__card">
                                <?php the_title('<a class="vil-search__card-title">', '</a>'); ?>

                                <div class="vil-search__card-text"><?php the_excerpt(); ?></div>

                                <div class="vil-search__card-info">
                                    <div class="vil-search__card-info-text"><?php esc_html_e('Article in', V_PREFIX); ?></div>
                                    <div class="vil-search__card-info-category-cont">
                                        <div class="vil-search__card-info-category-wrap">
                                            <span class="vil-search__card-info-category"><?php echo get_post_type(get_the_ID()); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; ?>
                        </div>

                        <div class="vil-more-posts__pagination">
                            <?php
                            global $wp_query;

                            $big = 999999999; // need an unlikely integer

                            echo paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var('paged') ),
                                'total' => $wp_query->max_num_pages
                            ) );
                            ?>
                        </div>
                    <?php else : ?>
                        <p>No results found for "<?php echo get_search_query(); ?>".</p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <script>
        function sort_search(value){
            window.location = '<?= $search_link_with_query; ?>&sort='+value;
        }
    </script>

<?php
get_footer();
