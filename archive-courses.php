<?php
/**
 * Courses archive template file
 */

get_header();


$page_header    = get_field('c_page_header', 'options');
$c_faq 	        = get_field('c_faq', 'options');
$f_courses      = get_field('featured_courses_listing', 'options');
$course_listing = get_field('course_listing', 'options');
$ready_to_enrol = get_field('c_ready_to_enroll', 'options');
$ready_to_enrol['id'] = 'ready-to-enroll';
$ready_to_enrol['wrapper_class'] = 'vil-ready-to-enroll__space';


get_template_part(
	'template-parts/elements/flexible-page-header', 
	null, 
	array(
		'title' => $page_header['title'], 
		'description' => $page_header['description'], 
		'primary_button' => $page_header['primary_button'], 
		'secondary_button' => $page_header['secondary_button'], 
		'media_column' => 'full',
        'id' => 'page-header'
	)
);


get_template_part(
    'template-parts/elements/featured-courses-listing', 
    null, 
    array(
        'title' => $f_courses['title'], 
        'anchor_title' => $f_courses['anchor_title'],
        'description' => '',
        'cta' => '', 
        'display_order' => $f_courses['display_order'], 
        'items' => $f_courses['items'], 
        'class' => 'vil-courses title-center vil-section-element',
        'id' => 'featured-courses',
    )
);


get_template_part(
    'template-parts/elements/full-filterable-course-listing', 
    null, 
    array(
        'title' => $course_listing['title'],
        'anchor_title' => $course_listing['anchor_title'],
        'count_items' => $course_listing['count_items'],
        'class' => 'vil-courses vil-section-element',
        'id' => 'course-listing'
    )
);


get_template_part( 'template-parts/blocks/ready-to-enroll', null, $ready_to_enrol );

if ( ! empty( $c_faq['items'] ) ) {
    get_template_part(
        'template-parts/blocks/faq',
        null,
        array(
            'title'  => $c_faq['title'],
            'anchor_title' => $c_faq['anchor_title'],
            'items'  => $c_faq['items'],
            'style'  => 'style-1',
            'anchor' => 'faq'
        )
    );
}

get_footer();
