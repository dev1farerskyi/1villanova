<?php
/**
 * 404 Page template.
 *
 * @package villanova
 * @since 1.0.0
 *
 */

get_header(); ?>
<div class="vil-block vil-section-element">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="vil-error">
                    <div class="vil-error__content">
                        <div class="vil-error-page__image mb-25">
                            <img class="" src="<?php echo ( get_template_directory_uri() ); ?>/assets/img/404.svg" alt="address">
                        </div>
                        <h1 class="vil-error__title mb-30"><?php esc_html_e( 'Page Not Found', V_PREFIX); ?></h1>
                        <h6 class="vil-error__description mb-25"><?php esc_html_e( 'We’re sorry, the page you requested could not be found Please go bake to the homepage', V_PREFIX); ?></h6>
                        <div class="vil-error__button">
                            <a href="<?php echo site_url('/'); ?>" class="vil-btn vil-btn_primary">
                                <?php esc_html_e('Go home', 'villanova', V_PREFIX); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
