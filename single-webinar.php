<?php
/**
 * Webinar post template
 */

get_header();

/**
 * Page header
 */

$title = get_field('title');
$subtitle = get_field('subtitle');
$description = get_field('description');
$primary_button = get_field('primary_button');
$date = get_field('date');
$date_full = get_field('date', false, false);
$time = get_field('time');
$type = get_field('type');
$recorded = get_field('recorded');
$not_recorded = get_field('not_recorded');
$live = get_field('live');
$form = get_field('form');
$speaker = get_field('speaker');
$media_column = get_field('media_column');

get_template_part('template-parts/elements/flexible-page-header', null, array(
    'title' => $title,
    'subtitle' => $subtitle,
    'description' => $description,
    'primary_button' => $primary_button,
    'secondary_button' => '',
    'media_column' => $media_column,
    'id' => 'page-header'
));
?>

<div class="vil-block-vebinar vil-section-element">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="vil-vebinar__main">
                    <?php the_post_thumbnail('large', array('class' => 'vil-vebinar__post_thumbnail')); ?>
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="vil-vebinar__register">
                    <div class="vil-vebinar__register-info">
                        <?php if (!empty($date)) : ?>
                            <div class="vil-vebinar__register-info-wrap">
                                <img src="<?php echo V_TEMP_URL . '/assets/img/icon-calendar.svg'; ?>" alt="icon">
                                <p class="vil-vebinar__register-info-text"><?php echo $date; ?></p>
                            </div>
                        <?php endif ?>

                        <?php if (!empty($time)) : ?>
                            <div class="vil-vebinar__register-info-wrap">
                                <img src="<?php echo V_TEMP_URL . '/assets/img/icon-clock.svg'; ?>" alt="icon">
                                <p class="vil-vebinar__register-info-text"><?php echo $time; ?></p>
                            </div>
                        <?php endif ?>
                        <?php if ($type === 'recorded') : ?>
                            <?php if (!empty($recorded)) : ?>
                                <div class="vil-vebinar__register-info-wrap">
                                    <img src="<?php echo V_TEMP_URL . '/assets/img/icon-record.svg'; ?>" alt="icon">
                                    <p class="vil-vebinar__register-info-text"><?php echo $recorded; ?></p>
                                </div>
                            <?php endif ?>
                        <?php elseif ($type === 'live') : ?>
                            <?php if (!empty($live)) : ?>
                                <div class="vil-vebinar__register-info-wrap">
                                    <img src="<?php echo V_TEMP_URL . '/assets/img/icon-record.svg'; ?>" alt="icon">
                                    <p class="vil-vebinar__register-info-text"><?php echo $live; ?></p>
                                </div>
                            <?php endif ?>
                        <?php elseif ($type === 'not_recorded') : ?>
                            <?php if (!empty($not_recorded)) : ?>
                                <div class="vil-vebinar__register-info-wrap">
                                    <img src="<?php echo V_TEMP_URL . '/assets/img/not-recorded.svg'; ?>" alt="icon">
                                    <p class="vil-vebinar__register-info-text"><?php echo $not_recorded; ?></p>
                                </div>
                            <?php endif ?>
                        <?php endif ?>
                    </div>
                    <?php if ( ! empty( $form ) && ! empty( $date_full ) ) : ?>
                        <?php if ( $date_full > date('Ymd') ): ?>
                            <div class="vil-vebinar__register-form">
                                <p class="vil-vebinar__register-title"><?php esc_html_e('Register Now', V_PREFIX); ?></p>
                                <?php echo do_shortcode('[gravityform id="' . $form['id'] . '" title="false" description="false"]'); ?>
                                
                            </div>
                        <?php else: ?>
                            <div class="vil-vebinar__register-form">
                                <p class="vil-vebinar__register-title closed"><?php esc_html_e('Registation Closed', V_PREFIX); ?></p>
                            </div>
                        <?php endif ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php if (!empty($speaker)) : ?>
            <div class="vil-block vil-vebinar__speakers">
                <h4 class="vil-vebinar__speakers-title"><?php esc_html_e('Meet the Speakers', V_PREFIX); ?></h4>
                <div class="vil-vebinar__speakers-wrap">
                    <div class="row justify-content-center">
                        <div class="col-xl-10">
                            <div class="row">
                                <?php foreach ($speaker as $row):
                                    $linkedin_url = $row['linkedin_url'];
                                    $twitter_url = $row['twitter_url'];
                                    $short_bio = $row['short_bio'];
                                    $name = $row['name'];
                                    $photo = $row['photo'];
                                    $link = $row['link'];
                                    ?>
                                    <div class="col-lg-4 col-md-4 col-sm-6">
                                        <div class="vil-vebinar__speakers-card">
                                            <?php if (!empty($photo)): ?>
                                                <img class="vil-vebinar__speakers-card-image"
                                                     src="<?php echo esc_url($photo['url']); ?>"
                                                     alt="<?php echo $photo['alt'] ?>">
                                            <?php endif ?>
                                            <?php if (!empty($name)) : ?>
                                                <p class="vil-vebinar__speakers-card-name"><?php echo $name; ?></p>
                                            <?php endif ?>
                                            <div class="vil-vebinar__speakers-card-socials">
                                                <?php if (!empty($linkedin_url)): ?>
                                                    <a class="vil-vebinar__speakers-card-socials-link"
                                                       href="<?php echo $linkedin_url; ?>" target="_blank">
                                                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/linkedin.svg"
                                                             alt="">
                                                    </a>
                                                <?php endif ?>
                                                <?php if (!empty($twitter_url)): ?>
                                                    <a class="vil-vebinar__speakers-card-socials-link"
                                                       href="<?php echo $twitter_url; ?>" target="_blank">
                                                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/assets/img/twitter.svg"
                                                             alt="">
                                                    </a>
                                                <?php endif ?>
                                            </div>
                                            <?php if (!empty($short_bio)) : ?>
                                                <p class="vil-vebinar__speakers-card-text"><?php echo $short_bio; ?></p>
                                            <?php endif ?>
                                            <?php vil_get_button($link, 'vil-vebinar__speakers-card-link'); ?>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>
    </div>
</div>

<?php
get_footer();